using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using PravoRu.DataLake.ArbitrageLoad.DataAccess.Model;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace PravoRu.DataLake.ArbitrageLoad.DataAccess.UnitTests
{
	[TestClass]
	public class ArbitrageContextApiTest
	{
		ArbitrageLoadContext _context;

		private DbSet<T> GetQueryableMockDbSet<T>(List<T> sourceList) where T : class
		{
			var queryable = sourceList.AsQueryable();

			var dbSet = new Mock<DbSet<T>>();
			dbSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(queryable.Provider);
			dbSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(queryable.Expression);
			dbSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
			dbSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(() => queryable.GetEnumerator());
			dbSet.Setup(d => d.Add(It.IsAny<T>())).Callback<T>((s) => sourceList.Add(s));

			return dbSet.Object;
		}

		[OneTimeSetUp]
		public void SetUp()
		{
			var moqContext = new Mock<ArbitrageLoadContext>();
			List<FrequencyBorders> FrequencyBordersMock = new List<FrequencyBorders>() {
				new FrequencyBorders(){
					LeftBorder= 0,Okved = 78, RightBorder= 0.5688
				},
				new FrequencyBorders(){
					LeftBorder= 0.442,Okved = 79, RightBorder= 0.5688
				}
			};
			List<Okved> okveds = new List<Okved>() {
				new Okved() { Description="�������� �����", OkvedCode = 77 }
			};
			List<Region> regions = new List<Region>() {
				new Region() { Description="�������� �������", RegionCode = 77 }
			};
			List<SideType> sides = new List<SideType>() {
				new SideType() { Description="�������� �������", Type = TypeSide.Defendants },
				new SideType() { Description="�������� �������", Type = TypeSide.Plaintiffs },
				new SideType() { Description="�������� �������", Type = TypeSide.Third }
			};

			List<ArbitrageStatistics> arbitrageStats = new List<ArbitrageStatistics>()
			{
				new ArbitrageStatistics() { AvgCountCase=10, Date=new DateTime(2015,1,5), OkvedCode= 77, Percentile50=5, Percentile75 = 7, Percentile90 = 9, Percentile95 = 9, RegionCode = 77, SideType = TypeSide.Defendants },
				new ArbitrageStatistics() { AvgCountCase=10, Date=new DateTime(2015,1,5), OkvedCode= 77, Percentile50=5, Percentile75 = 7, Percentile90 = 9, Percentile95 = 9, RegionCode = 77, SideType = TypeSide.Plaintiffs },
				new ArbitrageStatistics() { AvgCountCase=10, Date=new DateTime(2015,1,5), OkvedCode= 77, Percentile50=5, Percentile75 = 7, Percentile90 = 9, Percentile95 = 9, RegionCode = 77, SideType = TypeSide.Third }
			};

			List<ClaimStatistics> claimsStats = new List<ClaimStatistics>()
			{
				new ClaimStatistics() { AvgClaims=10000, Date=new DateTime(2015,1,5), OkvedCode= 77, Percentile50=5, Percentile75 = 7, Percentile90 = 9, Percentile95 = 9, RegionCode = 77, SumClaims = 6000000 },
				new ClaimStatistics() { AvgClaims=20000, Date=new DateTime(2015,1,5), OkvedCode= 77, Percentile50=5, Percentile75 = 7, Percentile90 = 9, Percentile95 = 9, RegionCode = 78, SumClaims = 6000000 },
				new ClaimStatistics() { AvgClaims=30000, Date=new DateTime(2015,1,5), OkvedCode= 77, Percentile50=5, Percentile75 = 7, Percentile90 = 9, Percentile95 = 9, RegionCode = 79, SumClaims = 6000000 }
			};

			moqContext.Setup(a => a.FrequencyBorders).Returns(GetQueryableMockDbSet(FrequencyBordersMock));
			moqContext.Setup(a => a.Okveds).Returns(GetQueryableMockDbSet(okveds));
			moqContext.Setup(a => a.Regions).Returns(GetQueryableMockDbSet(regions));
			moqContext.Setup(a => a.SideTypes).Returns(GetQueryableMockDbSet(sides));
			moqContext.Setup(a => a.ArbitrStat).Returns(GetQueryableMockDbSet(arbitrageStats));
			moqContext.Setup(a => a.ClaimStat).Returns(GetQueryableMockDbSet(claimsStats));
			_context = moqContext.Object;
		}

		[Test]
		public void ArbitrageContextApi_Constructor()
		{
			ArbitrageContextApi api = new ArbitrageContextApi(_context);
		}

		[TestCase(78)]
		[TestCase(79)]
		public void ArbitrageContext_GetFrequencyBordersByOkved_NotNull(int okved)
		{
			ArbitrageContextApi api = new ArbitrageContextApi(_context);
			var result = api.GetFrequencyBordersByOkved(okved);
			result.ShouldNotBeNull();
		}

		[TestCase(70)]
		[TestCase(71)]
		public void ArbitrageContext_GetFrequencyBordersByOkved_Null(int okved)
		{
			ArbitrageContextApi api = new ArbitrageContextApi(_context);
			var result = api.GetFrequencyBordersByOkved(okved);
			result.ShouldBeNull();
		}

		[Test]
		public void GetOkveds_ReturnOne()
		{
			ArbitrageContextApi api = new ArbitrageContextApi(_context);
			var result = api.GetOkveds();
			result.ShouldHaveSingleItem();
		}

		[Test]
		public void GetRegions_ReturnOne()
		{
			ArbitrageContextApi api = new ArbitrageContextApi(_context);
			var result = api.GetRegions();
			result.Count.ShouldBe(2);
		}

		[Test]
		public void GetSides_ReturnThree()
		{
			ArbitrageContextApi api = new ArbitrageContextApi(_context);
			var result = api.GetSides();
			result.Count().ShouldBe(3);
		}

		[Test]
		public void GetArbitrageStatisticses_ReturnEmpty()
		{
			ArbitrageContextApi api = new ArbitrageContextApi(_context);
			var result = api.GetArbitrageStatisticses(DateTime.Now, 54, 77, 0);
			result.ShouldBeEmpty();
			result = api.GetArbitrageStatisticses(DateTime.Now, DateTime.Now.AddDays(1), 77, 0);
			result.ShouldBeEmpty();
		}

		[Test]
		public void GetArbitrageStatisticses_ReturnValues()
		{
			ArbitrageContextApi api = new ArbitrageContextApi(_context);
			var result = api.GetArbitrageStatisticses(new DateTime(2015, 1, 5), 77, 77, 0);
			result.ShouldHaveSingleItem();
			result = api.GetArbitrageStatisticses(new DateTime(2015, 1, 1), new DateTime(2015, 1, 10), 77, 77);
			result.Count.ShouldBe(3);
		}

		[Test]
		public void GetClaimStatisticses_ReturnEmpty()
		{
			ArbitrageContextApi api = new ArbitrageContextApi(_context);
			var result = api.GetClaimStatisticses(new DateTime(2015, 1, 1), 77);
			result.ShouldBeEmpty();
			result = api.GetClaimStatisticses(new DateTime(2014, 1, 1), new DateTime(2014, 1, 10), 77, 77);
			result.ShouldBeEmpty();
		}

		[Test]
		public void GetClaimStatisticses_ReturnValues()
		{
			ArbitrageContextApi api = new ArbitrageContextApi(_context);
			var result = api.GetClaimStatisticses(new DateTime(2015, 1, 5), 77);
			result.ShouldHaveSingleItem();
			result = api.GetClaimStatisticses(new DateTime(2015, 1, 1), new DateTime(2015, 1, 10), 77, 77);
			result.ShouldHaveSingleItem();
		}
	}
}
