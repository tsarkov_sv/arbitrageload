using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Mapper;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Model;
using NUnit.Framework;

namespace PravoRu.DataLake.ArbitrageLoad.BusinessLogic.UnitTests
{
	[TestFixture]
	public class MapperTest
	{
		private readonly IServiceCollection collection = new ServiceCollection();

		[Test]
		public void Convert_DA_ArbitrageStatistics_To_ArbitrageStatistics()
		{
			DataAccess.Model.ArbitrageStatistics arbitrageStatistics = new DataAccess.Model.ArbitrageStatistics()
			{
				AvgCountCase = 10,
				Date = new System.DateTime(2015, 10, 10),
				OkvedCode = 77,
				Percentile50 = 5,
				Percentile75 = 7,
				Percentile90 = 9,
				Percentile95 = 9,
				RegionCode = 63,
				SideType = DataAccess.Model.TypeSide.Defendants
			};
			EtlMapperFactory etlMapperFactory = new EtlMapperFactory();
			var mapper = etlMapperFactory.Create();
			var statAfterMapper = mapper.Map<ArbitrageStatistics>(arbitrageStatistics);
			statAfterMapper.AvgCountCase.ShouldBe(arbitrageStatistics.AvgCountCase);
			statAfterMapper.Date.ShouldBe(arbitrageStatistics.Date);
			statAfterMapper.Okved.ShouldBe(arbitrageStatistics.OkvedCode);
			statAfterMapper.Percentile50.ShouldBe((long)arbitrageStatistics.Percentile50);
			statAfterMapper.Percentile75.ShouldBe((long)arbitrageStatistics.Percentile75);
			statAfterMapper.Percentile90.ShouldBe((long)arbitrageStatistics.Percentile90);
			statAfterMapper.Percentile95.ShouldBe((long)arbitrageStatistics.Percentile95);
			statAfterMapper.Region.ShouldBe(arbitrageStatistics.RegionCode.Value);
			statAfterMapper.SideType.ShouldBe(arbitrageStatistics.SideType);
		}

		[Test]
		public void Convert_DA_ClaimStatistics_To_ClaimStatistics()
		{
			DataAccess.Model.ClaimStatistics arbitrageStatistics = new DataAccess.Model.ClaimStatistics()
			{
				Date = new System.DateTime(2015, 10, 10),
				OkvedCode = 77,
				Percentile50 = 5,
				Percentile75 = 7,
				Percentile90 = 9,
				Percentile95 = 9,
				RegionCode = 63,
				AvgClaims = 120000,
				SumClaims = 130000
			};
			EtlMapperFactory etlMapperFactory = new EtlMapperFactory();
			var mapper = etlMapperFactory.Create();
			var statAfterMapper = mapper.Map<ClaimsStatistics>(arbitrageStatistics);
			statAfterMapper.Date.ShouldBe(arbitrageStatistics.Date);
			statAfterMapper.Okved.ShouldBe(arbitrageStatistics.OkvedCode);
			statAfterMapper.Percentile50.ShouldBe((long)arbitrageStatistics.Percentile50);
			statAfterMapper.Percentile75.ShouldBe((long)arbitrageStatistics.Percentile75);
			statAfterMapper.Percentile90.ShouldBe((long)arbitrageStatistics.Percentile90);
			statAfterMapper.Percentile95.ShouldBe((long)arbitrageStatistics.Percentile95);
			statAfterMapper.Region.ShouldBe(arbitrageStatistics.RegionCode.Value);
			statAfterMapper.AvgClaims.ShouldBe(arbitrageStatistics.AvgClaims);
			statAfterMapper.SumClaims.ShouldBe(arbitrageStatistics.SumClaims);
		}
	}
}
