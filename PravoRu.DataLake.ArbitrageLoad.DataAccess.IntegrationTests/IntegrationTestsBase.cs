using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Diagnostics;
using System.IO;

namespace PravoRu.DataLake.ArbitrageLoad.DataAccess.IntegrationTests
{
	public abstract class DbContextTestBase<T> where T : DbContext
	{
		public delegate void TestInitialized();

		public event TestInitialized TestInitializedEvent;

		protected static T DbContext;

		public virtual void TestInitialize()
		{
			var builder = new DbContextOptionsBuilder<T>();
			builder.UseInMemoryDatabase("TestDataBase");

			DbContext = CreateContext(builder.Options);
			TestInitializedEvent?.Invoke();
		}

		public virtual void TestCleanup()
		{
			DbContext.Database.EnsureDeleted();
			DbContext.Dispose();
			DbContext = null;
		}
		protected abstract T CreateContext(DbContextOptions<T> options);
	}
}