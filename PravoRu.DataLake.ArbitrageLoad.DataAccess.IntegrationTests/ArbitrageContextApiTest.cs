﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;
using System.Collections.Generic;
using PravoRu.DataLake.ArbitrageLoad.DataAccess.Model;
using NUnit.Framework;
using Microsoft.EntityFrameworkCore;

namespace PravoRu.DataLake.ArbitrageLoad.DataAccess.IntegrationTests
{
	[TestClass]
	public class ArbitrageContextApiTest : DbContextTestBase<ArbitrageLoadContext>
	{
		private ArbitrageLoadContext _context;

		[SetUp]
		public void Init()
		{
			TestInitialize();

			List<FrequencyBorders> frequencyBorders = new List<FrequencyBorders>() {
				new FrequencyBorders(){
					LeftBorder= 0,Okved = 78, RightBorder= 0.5688
				},
				new FrequencyBorders(){
					LeftBorder= 0.442,Okved = 79, RightBorder= 0.5688
				}
			};
			_context.FrequencyBorders.AddRange(frequencyBorders);

			List<Okved> okveds = new List<Okved>() {
				new Okved() { Description="Описание ОКВЭД", OkvedCode = 77 }
			};
			_context.Okveds.AddRange(okveds);

			List<Region> regions = new List<Region>() {
				new Region() { Description="Описание Региона", RegionCode = 77 }
			};
			_context.Regions.AddRange(regions);

			List<SideType> sides = new List<SideType>() {
				new SideType() { Description="Описание Стороны", Type = TypeSide.Defendants },
				new SideType() { Description="Описание Стороны", Type = TypeSide.Plaintiffs },
				new SideType() { Description="Описание Стороны", Type = TypeSide.Third }
			};
			_context.SideTypes.AddRange(sides);

			List<ArbitrageStatistics> arbitrageStats = new List<ArbitrageStatistics>()
			{
				new ArbitrageStatistics() { AvgCountCase=10, Date=new DateTime(2015,1,5), OkvedCode= 77, Percentile50=5, Percentile75 = 7, Percentile90 = 9, Percentile95 = 9, RegionCode = 77, SideType = TypeSide.Defendants },
				new ArbitrageStatistics() { AvgCountCase=10, Date=new DateTime(2015,1,5), OkvedCode= 77, Percentile50=5, Percentile75 = 7, Percentile90 = 9, Percentile95 = 9, RegionCode = 77, SideType = TypeSide.Plaintiffs },
				new ArbitrageStatistics() { AvgCountCase=10, Date=new DateTime(2015,1,5), OkvedCode= 77, Percentile50=5, Percentile75 = 7, Percentile90 = 9, Percentile95 = 9, RegionCode = 77, SideType = TypeSide.Third }
			};
			_context.ArbitrStat.AddRange(arbitrageStats);

			List<ClaimStatistics> claimsStats = new List<ClaimStatistics>()
			{
				new ClaimStatistics() { AvgClaims=10000, Date=new DateTime(2015,1,5), OkvedCode= 77, Percentile50=5, Percentile75 = 7, Percentile90 = 9, Percentile95 = 9, RegionCode = 77, SumClaims = 6000000 },
				new ClaimStatistics() { AvgClaims=20000, Date=new DateTime(2015,1,5), OkvedCode= 77, Percentile50=5, Percentile75 = 7, Percentile90 = 9, Percentile95 = 9, RegionCode = 78, SumClaims = 6000000 },
				new ClaimStatistics() { AvgClaims=30000, Date=new DateTime(2015,1,5), OkvedCode= 77, Percentile50=5, Percentile75 = 7, Percentile90 = 9, Percentile95 = 9, RegionCode = 79, SumClaims = 6000000 }
			};
			_context.ClaimStat.AddRange(claimsStats);

			_context.SaveChanges();
		}

		[TestCase(78)]
		[TestCase(79)]
		public void ArbitrageContext_GetFrequencyBordersByOkved_NotNull(int okved)
		{
			ArbitrageContextApi api = new ArbitrageContextApi(_context);
			var result = api.GetFrequencyBordersByOkved(okved);
			result.ShouldNotBeNull();
		}

		[TestCase(70)]
		[TestCase(71)]
		public void ArbitrageContext_GetFrequencyBordersByOkved_Null(int okved)
		{
			ArbitrageContextApi api = new ArbitrageContextApi(_context);
			var result = api.GetFrequencyBordersByOkved(okved);
			result.ShouldBeNull();
		}

		[Test]
		public void GetOkveds_ReturnOne()
		{
			ArbitrageContextApi api = new ArbitrageContextApi(_context);
			var result = api.GetOkveds();
			result.Count.ShouldBe(1);
		}

		[Test]
		public void GetRegions_ReturnTwo()
		{
			ArbitrageContextApi api = new ArbitrageContextApi(_context);
			var result = api.GetRegions();
			result.Count.ShouldBe(2);
		}

		[Test]
		public void GetSides_ReturnThree()
		{
			ArbitrageContextApi api = new ArbitrageContextApi(_context);
			var result = api.GetSides();
			result.Count().ShouldBe(3);
		}

		[Test]
		public void GetArbitrageStatisticses_ReturnEmpty()
		{
			ArbitrageContextApi api = new ArbitrageContextApi(_context);
			var result = api.GetArbitrageStatisticses(DateTime.Now, 54, 77, 0);
			result.ShouldBeEmpty();
			result = api.GetArbitrageStatisticses(DateTime.Now, DateTime.Now.AddDays(1), 77, 0);
			result.ShouldBeEmpty();
		}

		[Test]
		public void GetArbitrageStatisticses_ReturnValues()
		{
			ArbitrageContextApi api = new ArbitrageContextApi(_context);
			var result = api.GetArbitrageStatisticses(new DateTime(2015, 1, 5), 77, 77, 0);
			result.ShouldHaveSingleItem();
			result = api.GetArbitrageStatisticses(new DateTime(2015, 1, 1), new DateTime(2015, 1, 10), 77, 77);
			result.Count.ShouldBe(3);
		}

		[Test]
		public void GetClaimStatisticses_ReturnEmpty()
		{
			ArbitrageContextApi api = new ArbitrageContextApi(_context);
			var result = api.GetClaimStatisticses(new DateTime(2015, 1, 1), 77);
			result.ShouldBeEmpty();
			result = api.GetClaimStatisticses(new DateTime(2014, 1, 1), new DateTime(2014, 1, 10), 77, 77);
			result.ShouldBeEmpty();
		}

		[Test]
		public void GetClaimStatisticses_ReturnValues()
		{
			ArbitrageContextApi api = new ArbitrageContextApi(_context);
			var result = api.GetClaimStatisticses(new DateTime(2015, 1, 5), 77);
			result.ShouldHaveSingleItem();
			result = api.GetClaimStatisticses(new DateTime(2015, 1, 1), new DateTime(2015, 1, 10), 77, 77);
			result.ShouldHaveSingleItem();
		}

		protected override ArbitrageLoadContext CreateContext(DbContextOptions<ArbitrageLoadContext> options)
		{
			_context = new ArbitrageLoadContext(options);
			return _context;
		}

		[TearDown]
		public void TearDown()
		{
			TestCleanup();
		}
	}
}
