﻿using PravoRu.Common.ServiceDiscovery;
using System;
using System.Collections.Generic;
using System.Text;
using Hangfire;
using Hangfire.SqlServer;
using PravoRu.Common.ServiceDiscovery.Configuration;

namespace PravoRu.DataLake.ArbitrageLoad.Etl.Host
{
	/// <summary>
	/// Класс настроек Consul
	/// </summary>
	[SettingPath("DataLake/ArbitrageLoad")]
	public class HostSettings : ConsuledSettings, IHostSettings
	{
		/// <summary>
		/// .ctor
		/// </summary>
		public HostSettings()
		{
			ConfigurationStorageHangFire();
		}
		
		/// <summary>
		/// Строка соединения с БД ArbitrageLoad
		/// </summary>
		[DatabaseService(@"PravoRu.DataLake.ArbitrageLoad.Database", UserPassStorage = UserPassStorage.AppSettings)]
		public string ConnectionString { get; set; }

		/// <summary>
		/// Настройка крон триггера для HangFire
		/// </summary>
		[DefaultSettingValue("* * * * *")]
		public string CronTriggerStart { get; set; }

		/// <summary>
		/// Название задачи 
		/// </summary>
		[DefaultSettingValue("arbitrageload")]
		public string QueueName { get; set; }

		/// <summary>
		/// Интервал проверки заданий Hangfire
		/// </summary>
		[DefaultSettingValue("00:01:00")]
		public TimeSpan PollingInterval { get; set; }

		/// <inheritdoc/>
		public string GetConnectionString()
		{
			return ConnectionString;
		}

		/// <inheritdoc/>
		public string GetCronTriggerStart()
		{
			return CronTriggerStart;
		}

		/// <inheritdoc/>
		public string GetQueueName()
		{
			return QueueName;
		}

		/// <inheritdoc/>
		public BackgroundJobServer GetBackgroundJobServer()
		{
			var backgroundJobServerOptions = new BackgroundJobServerOptions
			{
				Queues = new[] {"default", "arbitrageload"},
				WorkerCount = 3,
				SchedulePollingInterval = PollingInterval
			};

			return new BackgroundJobServer(backgroundJobServerOptions);
		}

		/// <inheritdoc/>
		public RecurringJobManager GetRecurringJobManager()
		{
			return new RecurringJobManager();
		}

		private void ConfigurationStorageHangFire()
		{
			//Настройка базы HangFire
			JobStorage.Current = GlobalConfiguration.Configuration
				.UseSqlServerStorage(ConnectionString, new SqlServerStorageOptions()
				{
					SchemaName = "ArbitrageLoad"
				}).Entry;
		}
	}
}
