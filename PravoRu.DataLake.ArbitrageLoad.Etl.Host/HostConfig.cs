﻿using Microsoft.Extensions.DependencyInjection;
using NLog;
using PravoRu.DataLake.Common;

namespace PravoRu.DataLake.ArbitrageLoad.Etl.Host
{
	/// <summary>
	/// Класс конфигурирования Etl
	/// </summary>
	public class HostConfig : IServiceConfig
	{
		/// <summary>
		/// Метод конфигурирования
		/// </summary>
		/// <param name="collection"></param>
		public void Config(IServiceCollection collection)
		{
			collection
				.AddSingleton<ILogger>(p =>
				{
					var logger = LogManager.GetLogger("PravoRu.DataLake.ArbitrageLoad.Etl");
					return logger;
				})
				.AddSingleton<HostSettings>(new HostSettings());
		}
	}
}
