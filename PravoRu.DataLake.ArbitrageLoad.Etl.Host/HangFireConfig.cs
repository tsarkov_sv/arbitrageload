﻿using System;
using Hangfire;
using Hangfire.SqlServer;
using Microsoft.Extensions.DependencyInjection;
using PravoRu.DataLake.Common;

namespace PravoRu.DataLake.ArbitrageLoad.Etl.Host
{
	/// <summary>
	/// Класс настроек HangFire
	/// </summary>
	public class HangFireConfig : IServiceConfig
	{
		private HostSettings _settings;

		/// <summary>
		/// .ctor
		/// </summary>
		/// <param name="settings"></param>
		public HangFireConfig(HostSettings settings)
		{
			_settings = settings;
		}

		/// <summary>
		/// Метод конфигурирования hangfire
		/// </summary>
		/// <param name="collection"></param>
		public void Config(IServiceCollection collection)
		{
			var backgroundJobServerOptions = new BackgroundJobServerOptions
			{
				Queues = new[] { "default" },
				WorkerCount = 1
			};
			//Настройка HangFire на базу
			GlobalConfiguration.Configuration
				.UseSqlServerStorage(_settings.ConnectionString, new SqlServerStorageOptions()
				{
					SchemaName = "ArbitrageLoad"
				});

			collection.AddSingleton<IRecurringJobManager, RecurringJobManager>();
			collection.AddSingleton<BackgroundJobServer>(provider => new BackgroundJobServer(backgroundJobServerOptions));

		}
	}
}
