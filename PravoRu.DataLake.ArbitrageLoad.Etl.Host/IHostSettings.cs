﻿using Hangfire;

namespace PravoRu.DataLake.ArbitrageLoad.Etl.Host
{
	/// <summary>
	/// Интрефейс настроек хоста
	/// </summary>
	public interface IHostSettings
	{
		/// <summary>
		/// Получить строку соединения с БД ArbitrageLoad
		/// </summary>
		string GetConnectionString();

		/// <summary>
		/// Получить настройку крон триггера для HangFire
		/// </summary>
		string GetCronTriggerStart();

		/// <summary>
		/// Получение имени задания
		/// </summary>
		/// <returns></returns>
		string GetQueueName();

		/// <summary>
		/// Метод возвращает BackgroundJobServer HangFire
		/// </summary>
		/// <returns></returns>
		BackgroundJobServer GetBackgroundJobServer();

		/// <summary>
		/// Метод возвращает RecurringJobManager HangFire
		/// </summary>
		/// <returns></returns>
		RecurringJobManager GetRecurringJobManager();
	}
}