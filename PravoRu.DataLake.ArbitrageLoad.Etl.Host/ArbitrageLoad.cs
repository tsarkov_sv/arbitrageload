﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Hangfire;
using Hangfire.Common;
using NLog;

namespace PravoRu.DataLake.ArbitrageLoad.Etl.Host
{
	/// <summary>
	/// Класс арбитражной нагрузки
	/// </summary>
	public class ArbitrageLoad : IArbitrageLoad
	{
		private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();
		private readonly IRecurringJobManager _recurringJobManager;
		private readonly IHostSettings _settings;
		private readonly IEnumerable<IStatWorker> _workers;
		private readonly object _locker = new object();
		
		/// <summary>
		/// .ctor
		/// </summary>
		public ArbitrageLoad(IHostSettings settings, IEnumerable<IStatWorker> workers)
		{
			_settings = settings;
			_workers = workers;
			_recurringJobManager = settings.GetRecurringJobManager();
		}
		
		/// <inheritdoc/>
		public void Run()
		{
			var recurrentJob = Job.FromExpression<IArbitrageLoad>(us => us.RunSaveArbitrageStats());
			_recurringJobManager.AddOrUpdate("Запуск сбора статистики", recurrentJob,
				_settings.GetCronTriggerStart(), TimeZoneInfo.Utc,
				_settings.GetQueueName());
			_settings.GetBackgroundJobServer();
		}

		/// <inheritdoc/>
		public void RunSaveArbitrageStats()
		{
			if (!Monitor.TryEnter(_locker))
			{
				Logger.Warn("Задача \"Сбор статистики по арбитражной нагрузке\" уже запущена");
				return;
			}
			try
			{
				Monitor.Enter(_locker);
				Parallel.ForEach(_workers, new ParallelOptions() {MaxDegreeOfParallelism = Environment.ProcessorCount},
					worker => {
						try
						{
							worker.Run();
						}
						catch (Exception e)
						{
							Logger.Error(e, $"Ошибка сбора статистики {worker.GetType().Name}");
						}
					});
			}

			catch (Exception e)
			{
				Logger.Error(e);
			}
			finally
			{
				Monitor.Exit(_locker);
			}
		}
	}
}