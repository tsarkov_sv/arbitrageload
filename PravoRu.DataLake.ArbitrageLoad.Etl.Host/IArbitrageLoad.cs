﻿namespace PravoRu.DataLake.ArbitrageLoad.Etl.Host
{
	/// <summary>
	/// Интерфейс класса арбитражной нагрузки
	/// </summary>
	public interface IArbitrageLoad
	{
		/// <summary>
		/// Метод запуска сервиса
		/// </summary>
		void Run();

		/// <summary>
		/// Метод запуска сбора статистики
		/// </summary>
		void RunSaveArbitrageStats();
	}
}