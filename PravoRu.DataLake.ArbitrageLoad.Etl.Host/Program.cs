﻿using Hangfire;
using Microsoft.EntityFrameworkCore;
using NLog;
using PravoRu.Common.ServiceDiscovery;
using PravoRu.Common.ServiceDiscovery.NLog;
using PravoRu.DataLake.ArbitrageLoad.DataAccess;
using System;
using Autofac;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Mapper;

namespace PravoRu.DataLake.ArbitrageLoad.Etl.Host
{
	/// <summary>
	/// Класс Program
	/// </summary>
	public class Program
	{
		private static ILogger logger = LogManager.GetCurrentClassLogger();
		private static IContainer _container;

		/// <summary>
		/// main метод
		/// </summary>
		/// <param name="args"></param>
		public static void Main(string[] args)
		{
			//Настройка NLog
			ServiceDiscoveryFacade.Instance
				.Start<ArbitrageLoad>()
				.InitLogging()
				.RegisterDockerService();
			
			AutoMigrate();

			_container = GetContainer();

			GlobalConfiguration.Configuration.UseAutofacActivator(_container);
			JobActivator.Current = new AutofacJobActivator(_container);
			using (var scope = _container.BeginLifetimeScope())
			{
				scope.Resolve<IArbitrageLoad>().Run();
				Console.ReadLine();
			}
		}

		/// <summary>
		/// Метод формирования IoC контейнера
		/// </summary>
		/// <returns></returns>
		private static IContainer GetContainer()
		{
			var builder = new ContainerBuilder();
			builder.RegisterType<ArbitrageLoad>().As<IArbitrageLoad>().SingleInstance();
			builder.RegisterType<AvgStatWorker>().As<IStatWorker>().SingleInstance();
			builder.RegisterType<ClaimStatWorker>().As<IStatWorker>().SingleInstance();
			builder.RegisterType<HostSettings>().As<IHostSettings>().SingleInstance();
			builder.RegisterType<EtlSettings>().As<IEtlSettings>().SingleInstance();
			builder.RegisterType<ElasticSettings>().As<IElasticSettings>().SingleInstance();
			builder.RegisterType<EtlMapperFactory>().As<IEtlMapperFactory>().SingleInstance();
			builder.RegisterType<ArbitrageContextApi>().As<IArbitrageContextApi>().SingleInstance();
			builder.RegisterType<DataSettings>().As<IDataSettings>().SingleInstance();
			builder.RegisterType<ArbitrageLoadContext>().As<IArbitrageLoadContext>().SingleInstance();
			return builder.Build();
		}

		/// <summary>
		/// Метод реализации автомиграций
		/// </summary>
		private static void AutoMigrate()
		{
			using (var context = new ArbitrageLoadContext())
			{
				try
				{
					context.Database.Migrate();
				}
				catch (Exception e)
				{
					logger.Error(e, "Ошибка при накатывании миграции");
				}
			}
		}
	}
}