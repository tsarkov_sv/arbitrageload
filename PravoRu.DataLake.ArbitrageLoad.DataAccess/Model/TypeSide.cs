﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PravoRu.DataLake.ArbitrageLoad.DataAccess.Model
{
	/// <summary>
	/// Тип стороны
	/// </summary>
	public enum TypeSide
	{
		/// <summary>
		/// Истец (кредитор)
		/// </summary>
		Plaintiffs = 0,
		/// <summary>
		/// Ответчик (должник)
		/// </summary>
		Defendants = 1,
		/// <summary>
		/// Третье лицо
		/// </summary>
		Third = 2
	}
}
