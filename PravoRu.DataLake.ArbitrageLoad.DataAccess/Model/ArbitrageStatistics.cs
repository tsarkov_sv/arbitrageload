﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PravoRu.DataLake.ArbitrageLoad.DataAccess.Model
{
	/// <summary>
	/// Таблица арбитражной статистики
	/// </summary>
	public class ArbitrageStatistics
    {
		/// <summary>
		/// Идентификатор записи
		/// </summary>
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; set; }

		/// <summary>
		/// Месяц и год за который собрана статистика
		/// </summary>
		public DateTime Date { get; set; }

		/// <summary>
		/// Код региона по которому собрана статистика
		/// </summary>
		public int? RegionCode { get; set; }

		/// <summary>
		/// Отрасль по которой собрана статистика
		/// </summary>
		public int OkvedCode { get; set; }

		/// <summary>
		/// Среднее количество дел
		/// </summary>
		public long AvgCountCase { get; set; }

		/// <summary>
		/// Тип стороны по которой собиралась статистика
		/// </summary>
		public TypeSide SideType { get; set; }

		/// <summary>
		/// Значение 50 процентиля
		/// </summary>
		public double Percentile50 { get; set; }

		/// <summary>
		/// Значение 75 процентиля
		/// </summary>
		public double Percentile75 { get; set; }

		/// <summary>
		/// Значение 90 процентиля
		/// </summary>
		public double Percentile90 { get; set; }

		/// <summary>
		/// Значение 95 процентиля
		/// </summary>
		public double Percentile95 { get; set; }

		/// <summary>
		/// Значение 99 процентиля
		/// </summary>
		public double Percentile99 { get; set; }
	}
}
