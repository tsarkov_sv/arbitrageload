﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PravoRu.DataLake.ArbitrageLoad.DataAccess.Model
{
	/// <summary>
	/// Таблица ОКВЕДов
	/// </summary>
    public class Okved
    {
		/// <summary>
		/// Идентификатор
		/// </summary>
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; set; }

		/// <summary>
		/// Описание окведа
		/// </summary>
		[MaxLength(350)]
		public String Description { get; set; }

		/// <summary>
		/// Код оквед
		/// </summary>
		public int OkvedCode { get; set; }
	}
}
