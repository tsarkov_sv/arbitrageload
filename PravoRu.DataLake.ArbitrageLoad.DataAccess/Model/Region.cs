﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PravoRu.DataLake.ArbitrageLoad.DataAccess.Model
{
	/// <summary>
	/// Таблица регионов
	/// </summary>
    public class Region
    {
		/// <summary>
		/// Идентификатор
		/// </summary>
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; set; }

		/// <summary>
		/// Описание региона
		/// </summary>
		[MaxLength(350)]
		public String Description { get; set; }

		/// <summary>
		/// Код региона
		/// </summary>
		public int RegionCode { get; set; }
	}
}
