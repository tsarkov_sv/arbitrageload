﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PravoRu.DataLake.ArbitrageLoad.DataAccess.Model
{
	/// <summary>
	/// Таблица типов сторон
	/// </summary>
    public class SideType
    {
		/// <summary>
		/// Идентификатор
		/// </summary>
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

		/// <summary>
		/// Тип
		/// </summary>
		public TypeSide Type { get; set; }

		/// <summary>
		/// Описание стороны
		/// </summary>
		[MaxLength(150)]
		public string Description { get; set; }
	}

}
