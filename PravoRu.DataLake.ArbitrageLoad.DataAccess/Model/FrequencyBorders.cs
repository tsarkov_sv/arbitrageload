﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PravoRu.DataLake.ArbitrageLoad.DataAccess.Model
{
	/// <summary>
	/// Таблица границ частоты участия в судебных делах
	/// </summary>
    public class FrequencyBorders
    {
		/// <summary>
		/// Идентификатор записи
		/// </summary>
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

		/// <summary>
		/// ОКВЭД
		/// </summary>
		public int Okved { get; set; }

		/// <summary>
		/// Левая граница
		/// </summary>
		public double LeftBorder { get; set; }

		/// <summary>
		/// Правая граница
		/// </summary>
		public double RightBorder { get; set; }
	}
}
