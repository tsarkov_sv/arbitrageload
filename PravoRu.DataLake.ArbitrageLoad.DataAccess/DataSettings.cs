﻿using PravoRu.Common.ServiceDiscovery.Configuration;

namespace PravoRu.DataLake.ArbitrageLoad.DataAccess
{
	/// <summary>
	/// Класс настроек
	/// </summary>
	public class DataSettings : ConsuledSettings, IDataSettings
	{
		/// <summary>
		/// Настройка Consul
		/// </summary>
		[DatabaseService(@"PravoRu.DataLake.ArbitrageLoad.Database", UserPassStorage = UserPassStorage.AppSettings)]
		public string ConnectionString { get; set; }

		/// <summary>
		/// Метод получения строки подключения к БД
		/// </summary>
		/// <returns></returns>
		public string GetConnectionString()
		{
			return ConnectionString ?? "Data source=dl-dev.kad.local;initial catalog=ArbitrageLoad;integrated security=True;";
		}
	}
}
