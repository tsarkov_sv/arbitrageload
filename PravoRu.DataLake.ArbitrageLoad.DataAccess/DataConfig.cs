﻿using Microsoft.Extensions.DependencyInjection;
using PravoRu.DataLake.Common;

namespace PravoRu.DataLake.ArbitrageLoad.DataAccess
{
	/// <summary>
	/// Класс конфигуратор
	/// </summary>
	public class DataConfig : IServiceConfig
	{
		/// <summary>
		/// Метод конфигурирования
		/// </summary>
		/// <param name="collection"></param>
		public void Config(IServiceCollection collection)
		{
			collection
				.AddDbContext<ArbitrageLoadContext>()
				.AddSingleton<IDataSettings, DataSettings>()
				.AddSingleton<IArbitrageContextApi, ArbitrageContextApi>()
				.AddSingleton<IArbitrageLoadContext, ArbitrageLoadContext>();
		}
	}
}