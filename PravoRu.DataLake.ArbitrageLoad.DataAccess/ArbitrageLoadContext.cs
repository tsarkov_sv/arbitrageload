﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Polly;
using PravoRu.DataLake.ArbitrageLoad.DataAccess.Model;
using System;
using System.Threading;

namespace PravoRu.DataLake.ArbitrageLoad.DataAccess
{
	/// <summary>
	/// Контекст базы данных арбитражной нагрузки
	/// </summary>
	public class ArbitrageLoadContext : DbContext, IArbitrageLoadContext
	{
		private ILogger _logger = new LoggerFactory().CreateLogger(nameof(ArbitrageLoadContext));

		private readonly string _settings;

		/// <summary>
		/// .ctor
		/// </summary>
		public ArbitrageLoadContext()
		{
			IDataSettings settings = new DataSettings();
			_settings = settings.GetConnectionString();
		}

		/// <summary>
		/// .ctor
		/// </summary>
		/// <param name="settings"></param>
		public ArbitrageLoadContext(string settings)
		{
			_settings = settings;
		}

		/// <summary>
		/// .ctor
		/// </summary>
		/// <param name="settings"></param>
		public ArbitrageLoadContext(DbContextOptions<ArbitrageLoadContext> options) : base(options)
		{
		}

		/// <summary>
		/// Метод конфигурирования контекста
		/// </summary>
		/// <param name="optionsBuilder"></param>
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			if (!String.IsNullOrEmpty(_settings))
			{
				optionsBuilder
				.UseSqlServer(_settings,
				sqlServerOptionsAction: sqlOptions =>
				{
					sqlOptions.EnableRetryOnFailure(
					maxRetryCount: 5,
					maxRetryDelay: TimeSpan.FromSeconds(30),
					errorNumbersToAdd: null);
				});
			}
		}

		/// <summary>
		/// Метод создания модели
		/// </summary>
		/// <param name="modelBuilder"></param>
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			//Пример создания индексов
			modelBuilder.Entity<ArbitrageStatistics>()
				.HasIndex(p => new { p.OkvedCode, p.RegionCode }).HasName("IX_ArbitrageStatistics_OkvedRegion");
			modelBuilder.Entity<ArbitrageStatistics>()
				.HasIndex(p => p.Date).HasName("IX_ArbitrageStatistics_Date");
			modelBuilder.Entity<ArbitrageStatistics>()
				.HasIndex(p => new { p.OkvedCode, p.RegionCode, p.Date }).HasName("IX_ArbitrageStatistics_Full");
		}

		/// <summary>
		/// Таблица статистики
		/// </summary>
		public virtual DbSet<ArbitrageStatistics> ArbitrStat { get; set; }

		/// <summary>
		/// Таблица статистики по суммам взысканий
		/// </summary>
		public virtual DbSet<ClaimStatistics> ClaimStat { get; set; }

		/// <summary>
		/// Таблица Окведов
		/// </summary>
		public virtual DbSet<Okved> Okveds { get; set; }

		/// <summary>
		/// Таблица регионов
		/// </summary>
		public virtual DbSet<Region> Regions { get; set; }

		/// <summary>
		/// Таблица типов сторон
		/// </summary>
		public virtual DbSet<SideType> SideTypes { get; set; }

		/// <summary>
		/// Таблица границ частоты участия в судебных делах
		/// </summary>
		public virtual DbSet<FrequencyBorders> FrequencyBorders { get; set; }
	}
}
