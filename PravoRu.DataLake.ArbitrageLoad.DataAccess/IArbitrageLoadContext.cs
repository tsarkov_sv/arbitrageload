﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Internal;
using PravoRu.DataLake.ArbitrageLoad.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace PravoRu.DataLake.ArbitrageLoad.DataAccess
{
	/// <summary>
	/// Интерфейс DbContext
	/// </summary>
	public interface IArbitrageLoadContext
	{
		/// <summary>
		/// Таблица статистики арбитражной статистики
		/// </summary>
		DbSet<ArbitrageStatistics> ArbitrStat { get; set; }

		/// <summary>
		/// Таблица статистики по суммам взысканий
		/// </summary>
		DbSet<ClaimStatistics> ClaimStat { get; set; }

		/// <summary>
		/// Таблица Окведов
		/// </summary>
		DbSet<Okved> Okveds { get; set; }

		/// <summary>
		/// Таблица регионов
		/// </summary>
		DbSet<Region> Regions { get; set; }

		/// <summary>
		/// Таблица типов сторон
		/// </summary>
		DbSet<SideType> SideTypes { get; set; }

		/// <summary>
		/// Таблица границ частоты участия в судебных делах
		/// </summary>
		DbSet<FrequencyBorders> FrequencyBorders { get; set; }

		/// <summary>
		/// Сохранение изменений контекста
		/// </summary>
		/// <returns></returns>
		int SaveChanges();

		/// <summary>
		/// Сохранение изменений контекста
		/// </summary>
		/// <returns></returns>
		int SaveChanges(bool acceptAllChangesOnSuccess);
	}
}
