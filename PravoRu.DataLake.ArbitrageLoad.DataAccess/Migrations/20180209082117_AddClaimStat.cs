﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace PravoRu.DataLake.ArbitrageLoad.DataAccess.Migrations
{
    public partial class AddClaimStat : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ZIPS");

            migrationBuilder.CreateTable(
                name: "ClaimStat",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AvgClaims = table.Column<double>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    OkvedCode = table.Column<int>(nullable: false),
                    Percentile50 = table.Column<double>(nullable: false),
                    Percentile75 = table.Column<double>(nullable: false),
                    Percentile90 = table.Column<double>(nullable: false),
                    Percentile95 = table.Column<double>(nullable: false),
                    Percentile99 = table.Column<double>(nullable: false),
                    RegionCode = table.Column<int>(nullable: true),
                    SumClaims = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClaimStat", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClaimStat");

            migrationBuilder.CreateTable(
                name: "ZIPS",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Region = table.Column<string>(maxLength: 350, nullable: true),
                    RegionCode = table.Column<int>(nullable: false),
                    ZIPCode = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ZIPS", x => x.Id);
                });
        }
    }
}
