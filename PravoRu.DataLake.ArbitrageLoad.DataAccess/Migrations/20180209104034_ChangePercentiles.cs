﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace PravoRu.DataLake.ArbitrageLoad.DataAccess.Migrations
{
    public partial class ChangePercentiles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "Percentile95",
                table: "ArbitrStat",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<double>(
                name: "Percentile90",
                table: "ArbitrStat",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<double>(
                name: "Percentile75",
                table: "ArbitrStat",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<double>(
                name: "Percentile50",
                table: "ArbitrStat",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<double>(
                name: "Percentile99",
                table: "ArbitrStat",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Percentile99",
                table: "ArbitrStat");

            migrationBuilder.AlterColumn<long>(
                name: "Percentile95",
                table: "ArbitrStat",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<long>(
                name: "Percentile90",
                table: "ArbitrStat",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<long>(
                name: "Percentile75",
                table: "ArbitrStat",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<long>(
                name: "Percentile50",
                table: "ArbitrStat",
                nullable: false,
                oldClrType: typeof(double));
        }
    }
}
