﻿using System;
using System.Collections.Generic;
using PravoRu.DataLake.ArbitrageLoad.DataAccess.Model;

namespace PravoRu.DataLake.ArbitrageLoad.DataAccess
{
	/// <summary>
	/// Интерфейс API для работы с БД
	/// </summary>
	public interface IArbitrageContextApi
	{
		/// <summary>
		/// Получение границ частот по ОКВЭД
		/// </summary>
		/// <param name="okved">ОКВЭД</param>
		/// <returns></returns>
		FrequencyBorders GetFrequencyBordersByOkved(int okved);

		/// <summary>
		/// Метод получения списка ОКВЭД
		/// </summary>
		/// <returns></returns>
		List<int> GetOkveds();

		/// <summary>
		/// Метод получения регионов
		/// </summary>
		/// <returns></returns>
		List<int?> GetRegions();

		/// <summary>
		/// Метод получения сторон
		/// </summary>
		/// <returns></returns>
		List<int> GetSides();

		/// <summary>
		/// Метод получения данных по статистике
		/// </summary>
		/// <param name="date">Дата</param>
		/// <param name="okved">ОКВЭД</param>
		/// <param name="region">Номер региона</param>
		/// <param name="side">Тип участия</param>
		/// <returns></returns>
		List<ArbitrageStatistics> GetArbitrageStatisticses(DateTime date, int okved, int? region, int side);

		/// <summary>
		/// Удаление статистики за период
		/// </summary>
		/// <param name="list">Период статистики</param>
		void RemoveRangeArbitrStat(List<ArbitrageStatistics> list);

		/// <summary>
		/// Удаление статистики за период
		/// </summary>
		/// <param name="date"></param>
		/// <param name="okved"></param>
		/// <param name="region"></param>
		/// <param name="side"></param>
		void RemoveRangeArbitrStat(DateTime date, int okved, int? region, int side);

		/// <summary>
		/// Добавление данных по статистике
		/// </summary>
		/// <param name="list"></param>
		void AddRangeArbitrStat(List<ArbitrageStatistics> list);

		/// <summary>
		/// Получение статистики по параметрам
		/// </summary>
		/// <param name="startDate">Дата начала</param>
		/// <param name="endDate">Дата завершения</param>
		/// <param name="okved">ОКВЭД</param>
		/// <param name="region">Номер региона</param>
		/// <returns></returns>
		List<ArbitrageStatistics> GetArbitrageStatisticses(DateTime? startDate, DateTime? endDate, int okved,
			int? region);

		/// <summary>
		/// Добавление данных по суммам взысканий
		/// </summary>
		/// <param name="list"></param>
		void AddRangeClaimStat(List<ClaimStatistics> list);

		/// <summary>
		/// Метод получения данных по статистике сумм взыскания
		/// </summary>
		/// <param name="date">Дата</param>
		/// <param name="region">Номер региона</param>
		/// <param name="okved">ОКВЭД</param>
		/// <returns></returns>
		List<ClaimStatistics> GetClaimStatisticses(DateTime date, int? region);
		
		/// <summary>
		/// Удаление статистики сумм взысканий за период
		/// </summary>
		/// <param name="list">Период статистики</param>
		void RemoveRangeClaimStat(List<ClaimStatistics> list);

		/// <summary>
		/// Удаление статистики сумм взысканий за период
		/// </summary>
		/// <param name="date">Дата</param>
		/// <param name="region">Номер региона</param>
		/// <param name="okved">ОКВЭД</param>
		void RemoveRangeClaimStat(DateTime date, int? region);

		/// <summary>
		/// Получение исковой нагрузки по параметрам
		/// </summary>
		/// <param name="startDate">Дата начала</param>
		/// <param name="endDate">Дата завершения</param>
		/// <param name="okved">ОКВЭД</param>
		/// <param name="region">Номер региона</param>
		/// <returns></returns>
		List<ClaimStatistics> GetClaimStatisticses(DateTime? startDate, DateTime? endDate, int okved,
			int? region);
	}
}