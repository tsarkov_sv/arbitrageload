﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using PravoRu.DataLake.ArbitrageLoad.DataAccess.Model;

namespace PravoRu.DataLake.ArbitrageLoad.DataAccess
{
	/// <summary>
	/// Класс API для доступа к БД
	/// </summary>
	public class ArbitrageContextApi : IArbitrageContextApi
	{
		private ILogger logger = LogManager.GetCurrentClassLogger();
		readonly IArbitrageLoadContext _context;

		/// <summary>
		/// .ctor
		/// </summary>
		/// <param name="context"></param>
		public ArbitrageContextApi(IArbitrageLoadContext context)
		{
			_context = context;
		}

		/// <summary>
		/// Метод получения границ по ОКВЭД
		/// </summary>
		/// <param name="okved"></param>
		/// <returns></returns>
		public FrequencyBorders GetFrequencyBordersByOkved(int okved)
		{
			try
			{
				return _context.FrequencyBorders.FirstOrDefault(a => a.Okved == okved);
			}
			catch (Exception e)
			{
				logger.Error(e);
				return null;
			}
		}

		/// <inheritdoc/>
		public List<int> GetOkveds()
		{

			return _context.Okveds.Select(a => a.OkvedCode).ToList();
		}

		/// <inheritdoc/>
		public List<int?> GetRegions()
		{
			List<int?> listRegions = new List<int?>();
			listRegions.AddRange(_context.Regions.Select(a => (int?)a.RegionCode).ToList());
			listRegions.Add(null);
			return listRegions;
		}

		/// <inheritdoc/>
		public List<int> GetSides()
		{
			return _context.SideTypes.Select(a => (int)a.Type).ToList();
		}

		/// <inheritdoc/>
		public List<ArbitrageStatistics> GetArbitrageStatisticses(DateTime? startDate, DateTime? endDate, int okved,
			int? region)
		{
			return _context
				.ArbitrStat
				.Where(a => a.Date >= startDate && a.Date <= endDate)
				.Where(a => a.OkvedCode == okved)
				.Where(a => a.RegionCode == region).ToList();
		}

		/// <inheritdoc/>
		public List<ArbitrageStatistics> GetArbitrageStatisticses(DateTime date, int okved, int? region, int side)
		{
			return _context.ArbitrStat.Where(a =>
				a.Date == date && a.OkvedCode == okved && a.RegionCode == region &&
				a.SideType == (TypeSide)side).ToList();
		}

		/// <inheritdoc/>
		public void RemoveRangeArbitrStat(List<ArbitrageStatistics> list)
		{
			var listId = list.Select(a => a.Id);
			var listInstances = _context.ArbitrStat.Where(a => listId.Contains(a.Id));
			_context.ArbitrStat.RemoveRange(listInstances);
			_context.SaveChanges();
		}

		/// <inheritdoc/>
		public void AddRangeArbitrStat(List<ArbitrageStatistics> list)
		{
			_context.ArbitrStat.AddRange(list);
			_context.SaveChanges();
		}

		/// <inheritdoc/>
		public void AddRangeClaimStat(List<ClaimStatistics> list)
		{
			_context.ClaimStat.AddRange(list);
			_context.SaveChanges();
		}

		/// <inheritdoc/>
		public List<ClaimStatistics> GetClaimStatisticses(DateTime date, int? region)
		{
			return _context.ClaimStat.Where(a =>
				a.Date == date && a.RegionCode == region).ToList();
		}

		/// <inheritdoc/>
		public void RemoveRangeClaimStat(List<ClaimStatistics> list)
		{
			var listId = list.Select(a => a.Id);
			var listInstances = _context.ClaimStat.Where(a => listId.Contains(a.Id));
			_context.ClaimStat.RemoveRange(listInstances);
			_context.SaveChanges();
		}

		/// <inheritdoc/>
		public void RemoveRangeClaimStat(DateTime date, int? region)
		{
			var listInstances = _context.ClaimStat.Where(a => a.Date == date && a.RegionCode == region);
			_context.ClaimStat.RemoveRange(listInstances);
			_context.SaveChanges();
		}

		/// <inheritdoc/>
		public List<ClaimStatistics> GetClaimStatisticses(DateTime? startDate, DateTime? endDate, int okved, int? region)
		{
			return _context
				.ClaimStat
				.Where(a => a.Date >= startDate && a.Date <= endDate)
				.Where(a => a.OkvedCode == okved)
				.Where(a => a.RegionCode == region).ToList();
		}

		/// <inheritdoc/>
		public void RemoveRangeArbitrStat(DateTime date, int okved, int? region, int side)
		{
			var listInstances = _context.ArbitrStat.Where(a =>
				a.Date == date && a.OkvedCode == okved && a.RegionCode == region &&
				a.SideType == (TypeSide)side);
			_context.ArbitrStat.RemoveRange(listInstances);
			_context.SaveChanges();
		}
	}
}