﻿namespace PravoRu.DataLake.ArbitrageLoad.DataAccess
{
	/// <summary>
	/// Класс настроек
	/// </summary>
	public interface IDataSettings
	{
		/// <summary>
		/// Метод получения строки подключения к БД
		/// </summary>
		/// <returns></returns>
		string GetConnectionString();
	}
}
