﻿using Nest;
using PravoRu.Common.ServiceDiscovery.Configuration;
using PravoRu.Common.Elastic.Search.CommonRealizations;

namespace PravoRu.DataLake.ArbitrageLoad.BusinessLogic
{
	/// <summary>
	/// Класс настроек elasticSearch
	/// </summary>
	[SettingPath("DataLake/ArbitrageLoad")]
	public class ElasticSettings : ConsuledSettings, IElasticSettings
	{
		/// <summary>
		/// Настройка строк соединения с elastic search
		/// </summary>
		[ConsulService("Elastic", true)]
		public string[] ElasticConnectionStrings { get; set; }

		/// <summary>
		/// Настройка индекса 
		/// </summary>
		[DefaultSettingValue("cases")]
		public string ElasticIndex { get; set; }

		/// <summary>
		/// Настройка индекса организаций
		/// </summary>
		[DefaultSettingValue("organizations_v4")]
		public string ElasticOrgIndex { get; set; }

		/// <summary>
		/// Метод возвращает клиент elasticSearch для индекса organizations_v4
		/// </summary>
		/// <returns></returns>

		public IElasticClient GetElasticOrgClient()
		{
			var factory = new ElasticClientFactoryStaticPool(ElasticConnectionStrings, ElasticOrgIndex);
			return factory.GetClient();
		}

		/// <summary>
		/// Метод возвращает клиент elasticSearch для индекса cases
		/// </summary>
		/// <returns></returns>
		public IElasticClient GetElasticCaseClient()
		{
			var factory = new ElasticClientFactoryStaticPool(ElasticConnectionStrings, ElasticIndex);
			return factory.GetClient();
		}
	}
}
