﻿using AutoMapper;

namespace PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Mapper
{
	/// <summary>
	/// Фабрика AutoMapper
	/// </summary>
	public interface IEtlMapperFactory
	{
		/// <summary>
		/// Метод получения mapper
		/// </summary>
		/// <returns></returns>
		IMapper Create();
	}
}
