﻿using AutoMapper;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Model;
using PravoRu.DataLake.ArbitrageLoad.DataAccess.Model;
using ArbitrageStatistics = PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Model.ArbitrageStatistics;

namespace PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Mapper
{
	/// <summary>
	/// Фабрика AutoMapper
	/// </summary>
	public class EtlMapperFactory : IEtlMapperFactory
	{
		private readonly MapperConfiguration _mapperConfiguration;

		/// <summary>
		/// .ctor
		/// </summary>
		public EtlMapperFactory()
		{
			_mapperConfiguration = new MapperConfiguration(ConfigureMappings);
		}

		private void ConfigureMappings(IMapperConfigurationExpression mapperConfigurationExpression)
		{
			ConfigureBase(mapperConfigurationExpression);
		}

		private void ConfigureBase(IMapperConfigurationExpression mapperConfigurationExpression)
		{
			mapperConfigurationExpression.CreateMap<DataAccess.Model.ArbitrageStatistics, ArbitrageStatistics>()
				.ForMember(pfd => pfd.Okved, c => c.MapFrom(s => s.OkvedCode))
				.ForMember(pfd => pfd.Region, c => c.MapFrom(s => s.RegionCode));

			mapperConfigurationExpression.CreateMap<DataAccess.Model.ClaimStatistics, ClaimsStatistics>()
				.ForMember(pfd => pfd.Okved, c => c.MapFrom(s => s.OkvedCode))
				.ForMember(pfd => pfd.Region, c => c.MapFrom(s => s.RegionCode));
		}

		/// <summary>
		/// Метод получения mapper
		/// </summary>
		/// <returns></returns>
		public IMapper Create()
		{
			return _mapperConfiguration.CreateMapper();
		}
	}
}