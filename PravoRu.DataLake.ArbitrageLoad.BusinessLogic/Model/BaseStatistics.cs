﻿using System;

namespace PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Model
{
	/// <summary>
	/// Базовый класс статистики
	/// </summary>
	public class BaseStatistics
	{
		/// <summary>
		/// Значение 50 процентиля
		/// </summary>
		public long Percentile50 { get; set; }

		/// <summary>
		/// Значение 75 процентиля
		/// </summary>
		public long Percentile75 { get; set; }

		/// <summary>
		/// Значение 90 процентиля
		/// </summary>
		public long Percentile90 { get; set; }

		/// <summary>
		/// Значение 95 процентиля
		/// </summary>
		public long Percentile95 { get; set; }

		/// <summary>
		/// Месяц и год за который собрана статистика
		/// </summary>
		public DateTime Date { get; set; }

		/// <summary>
		/// Код региона по которому собрана статистика
		/// </summary>
		public int Region { get; set; }

		/// <summary>
		/// Отрасль по которой собрана статистика
		/// </summary>
		public int Okved { get; set; }
	}
}