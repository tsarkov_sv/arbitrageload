﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Model
{
	/// <summary>
	/// Модель дела
	/// </summary>
	public class Case
	{
		/// <summary>
		/// Дата регистрации дела
		/// </summary>
		public DateTime? RegistrationDate { get; set; }
	}
}
