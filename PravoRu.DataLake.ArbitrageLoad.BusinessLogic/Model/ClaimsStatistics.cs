﻿namespace PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Model
{
	/// <summary>
	/// Статистика по суммам взысканий
	/// </summary>
	public class ClaimsStatistics : BaseStatistics
	{
		/// <summary>
		/// Средняя исковая нагрузка
		/// </summary>
		public double AvgClaims { get; set; }

		/// <summary>
		/// Общая исковая нагрузка
		/// </summary>
		public double SumClaims { get; set; }
	}
}