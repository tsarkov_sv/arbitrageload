﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Model
{
	/// <summary>
	/// Класс ОКВЭД кода
	/// </summary>
	public class OkvedCode
	{
		/// <summary>
		/// Код
		/// </summary>
		public string Code { get; set; }
		/// <summary>
		/// Признак основного
		/// </summary>
		public bool IsMain { get; set; }
	}
}
