﻿using System;
using System.Collections.Generic;

namespace PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Model
{
	/// <summary>
	/// Класс - индекс
	/// </summary>
	public class FnsOrganization
	{
		/// <summary>
		/// Дата регистрации организации
		/// </summary>
		public DateTime? RegistrationDate { get; set; }
		/// <summary>
		/// Коды ОКВЭДов
		/// </summary>
		public List<OkvedCode> OkvedsCodes { get; set; }

		/// <summary>
		/// Отрасль
		/// </summary>
		public Industry Industry { get; set; }
	}
}
