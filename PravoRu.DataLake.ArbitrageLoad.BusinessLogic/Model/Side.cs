﻿using System;

namespace PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Model
{
	/// <summary>
	/// Модель стороны дела
	/// </summary>
	public class Side
    {
		/// <summary>
		/// Дата регистрации дела
		/// </summary>
		public DateTime? CaseRegistrationDate { get; set; }

		/// <summary>
		/// ОКВЭД
		/// </summary>
		public string MainOkved { get; set; }

		/// <summary>
		/// Сумма требований
		/// </summary>
		public double ClaimSum { get; set; }
	}
}
