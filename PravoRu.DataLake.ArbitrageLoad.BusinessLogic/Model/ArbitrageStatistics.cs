﻿using PravoRu.DataLake.ArbitrageLoad.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Model
{
	/// <summary>
	/// Статистика по ОКВЕД,Региону,Типу участника дела, За период
	/// </summary>
	public class ArbitrageStatistics : BaseStatistics
	{
		/// <summary>
		/// Среднее количество дел
		/// </summary>
		public long AvgCountCase { get; set; }

		/// <summary>
		/// Тип стороны по которой собиралась статистика
		/// </summary>
		public TypeSide SideType { get; set; }
	}
}
