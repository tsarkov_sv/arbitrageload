﻿namespace PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Model
{
	/// <summary>
	/// Отрасль
	/// </summary>
	public class Industry
	{
		/// <summary>
		/// Код отрасли
		/// </summary>
		public string Code { get; set; }

		/// <summary>
		/// Наименование отрасли
		/// </summary>
		public string Name { get; set; }
	}
}