﻿using Nest;

namespace PravoRu.DataLake.ArbitrageLoad.BusinessLogic
{
	/// <summary>
	/// Настроеки elasticSearch
	/// </summary>
	public interface IElasticSettings
	{
		/// <summary>
		/// Настройка строк соединения с elastic search
		/// </summary>
		string[] ElasticConnectionStrings { get; set; }

		/// <summary>
		/// Настройка индекса 
		/// </summary>
		string ElasticIndex { get; set; }

		/// <summary>
		/// Настройка индекса организаций
		/// </summary>
		string ElasticOrgIndex { get; set; }

		/// <summary>
		/// Получение клиента cases Elastic Search
		/// </summary>
		/// <returns></returns>
		IElasticClient GetElasticCaseClient();

		/// <summary>
		/// Получение клиента organizations Elastic Search
		/// </summary>
		/// <returns></returns>
		IElasticClient GetElasticOrgClient();
	}
}