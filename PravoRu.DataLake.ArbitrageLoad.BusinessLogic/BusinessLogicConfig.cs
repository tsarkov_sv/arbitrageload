﻿using Microsoft.Extensions.DependencyInjection;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Mapper;
using PravoRu.DataLake.Common;

namespace PravoRu.DataLake.ArbitrageLoad.BusinessLogic
{
	/// <summary>
	/// Класс конфигуратор
	/// </summary>
	public class BusinessLogicConfig : IServiceConfig
	{
		/// <summary>
		/// Метод конфигурирования
		/// </summary>
		/// <param name="collection"></param>
		public void Config(IServiceCollection collection)
		{
			collection
				.AddSingleton<IEtlMapperFactory, EtlMapperFactory>();
		}
	}
}