﻿using System;
using System.Net;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog;
using PravoRu.Common.Extensions;
using PravoRu.Common.ServiceDiscovery;
using PravoRu.Common.ServiceDiscovery.NLog;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Mapper;
using PravoRu.DataLake.ArbitrageLoad.DataAccess;

namespace PravoRu.DataLake.ArbitrageLoad.Api.Host
{
	/// <summary>
	/// 
	/// </summary>
	public class Startup : PravoRu.Common.WebApi.StartupBase
	{
		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="env"></param>
		public Startup(Microsoft.AspNetCore.Hosting.IHostingEnvironment env) : base(env)
		{
		}

		protected override string SwaggerTitle => "PravoRu.DataLake.ArbitrageLoad";

		protected override string SwaggerDescription => "Микросервис арбитражной нагрузки";

		/// This method gets called by the runtime. Use this method to add services to the container.
		public override void ConfigureServices(IServiceCollection services)
		{
			services.AddSingleton<IElasticSettings, ElasticSettings>();
			services.AddSingleton<IDataSettings, DataSettings>();
			services.AddSingleton<IArbitrageLoadApi, ArbitrageLoadApi>();
			services.AddSingleton<IEtlMapperFactory, EtlMapperFactory>();
			services.AddSingleton<IArbitrageContextApi, ArbitrageContextApi>();
			services.AddSingleton<IArbitrageLoadContext, ArbitrageLoadContext>();
			base.ConfigureServices(services);
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		/// <inheritdoc />
		public override void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
		{
			ServiceDiscoveryFacade.Instance
				.Start(this)
				.InitLogging();

			var pathBase = Environment.GetEnvironmentVariable("PATH_BASE");
			app.UsePathBase(pathBase.NullOrEmpty() ? "/ArbitrageLoad" : pathBase);
			base.Configure(app, env, loggerFactory);

			LogManager.GetCurrentClassLogger().Info("WebApi started");
		}
	}
}