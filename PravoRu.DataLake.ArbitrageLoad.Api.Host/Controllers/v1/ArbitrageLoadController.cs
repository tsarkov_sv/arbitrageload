﻿using Microsoft.AspNetCore.Mvc;
using PravoRu.DataLake.ArbitrageLoad.Api.Model;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Model;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Net;
using ArbitrageStatistics = PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Model.ArbitrageStatistics;

namespace PravoRu.DataLake.ArbitrageLoad.Api.Host.Controllers.v1
{
	/// <summary>
	/// Контроллер
	/// </summary>
	[Route("api/[controller]/[action]")]
	public class ArbitrageLoadController : Controller
	{
		private static DateTime _from = DateTime.Now.AddMonths(-6);
		private IArbitrageLoadApi _api;

		/// <summary>
		/// Конструктор
		/// </summary>
		/// <param name="api"></param>
		public ArbitrageLoadController(IArbitrageLoadApi api)
		{
			_api = api;
		}

		/// <summary>
		/// Выдает статистику арбитражной нагрузки по ОКВЭД, Региону и стороне за указанный период
		/// </summary>
		/// <param name="request">Модель запроса статистики</param>
		/// <returns></returns>
		[SwaggerResponse((int) HttpStatusCode.OK, Type = typeof(StatResponse<ArbitrageStatistics>))]
		[SwaggerResponse((int) HttpStatusCode.BadRequest, Type = typeof(string))]
		[HttpPost]
		public IActionResult StatArbitrageByOkvedRegion(ArbitrageStatRequest request)
		{
			if (request == null || !request.Validate())
			{
				return BadRequest("Модель запроса статистики не прошла валидацию.");
			}
			return Ok(new StatResponse<ArbitrageStatistics>() { Statistics = _api.GetArbitrageStatistic(request)});
		}

		/// <summary>
		/// Выдает признак частоты участия компании в судебных делах
		/// </summary>
		/// <param name="request">Модель запроса статистики</param>
		/// <returns></returns>
		[SwaggerResponse((int) HttpStatusCode.OK, Type = typeof(FrequencyResponse))]
		[SwaggerResponse((int) HttpStatusCode.BadRequest, Type = typeof(string))]
		[HttpPost]
		public IActionResult GetFrequencyAttribute(FrequencyRequest request)
		{
			if (request == null || !request.Validate())
			{
				return BadRequest(
					"Модель запроса признака частоты участия компании в судебных делах не прошла валидацию.");
			}
			return Ok(new FrequencyResponse() {Frequency = _api.GetFrequencyAttribute(request.OrgInfo)});
		}

		/// <summary>
		/// Выдает статистику исковой нагрузки по ОКВЭД и Региону за указанный период
		/// </summary>
		/// <param name="request">Модель запроса статистики</param>
		/// <returns></returns>
		[SwaggerResponse((int) HttpStatusCode.OK, Type = typeof(StatResponse<ClaimsStatistics>))]
		[SwaggerResponse((int) HttpStatusCode.BadRequest, Type = typeof(string))]
		[HttpPost]
		public IActionResult StatClaimByOkvedRegion(ClaimStatRequest request)
		{
			if (request == null || !request.Validate())
			{
				return BadRequest("Модель запроса статистики не прошла валидацию.");
			}
			return Ok(new StatResponse<ClaimsStatistics>() { Statistics = _api.GetClaimStatistic(request)});
		}
	}
}