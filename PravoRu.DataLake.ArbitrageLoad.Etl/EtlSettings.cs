﻿using Elasticsearch.Net;
using Nest;
using PravoRu.Common.Configuration;
using PravoRu.Common.Elastic.Search.CommonRealizations;
using PravoRu.Common.ServiceDiscovery.Configuration;
using System;
using System.Globalization;

namespace PravoRu.DataLake.ArbitrageLoad.Etl
{
	/// <summary>
	/// Класс настроек
	/// </summary>
	[SettingPath(@"DataLake/ArbitrageLoad")]
	public class EtlSettings : ConsuledSettings, IEtlSettings
	{
		/// <summary>
		/// Строка соединения с БД Арбитражной нагрузки
		/// </summary>
		[DatabaseService(@"PravoRu.DataLake.ArbitrageLoad.Database", UserPassStorage = UserPassStorage.AppSettings)]
		public string ConnectionString { get; set; }

		/// <summary>
		/// Получение строки соединения
		/// </summary>
		/// <returns></returns>
		public string GetConnectionString()
		{
			return ConnectionString;
		}

		/// <summary>
		/// Дата последнего обновления статистики
		/// </summary>
		[DefaultSettingValue("2007-01-01")]
		public string StartDateLoad { get; set; }

		/// <summary>
		/// Получение даты с которой необходимо собрать статистику
		/// </summary>
		/// <returns></returns>
		public DateTime GetStartDateLoad()
		{
			return DateTime.ParseExact(StartDateLoad, "yyyy-MM-dd", CultureInfo.InvariantCulture);
		}

		/// <summary>
		/// Сохранение даты последнего сбора статистики
		/// </summary>
		/// <param name="date"></param>
		public void SaveStartDateLoad(DateTime date)
		{
			StartDateLoad = date.ToString("yyyy-MM-dd");
			this.Save();
		}
	}
}
