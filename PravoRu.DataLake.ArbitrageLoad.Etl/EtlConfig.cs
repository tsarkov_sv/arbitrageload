﻿using Microsoft.Extensions.DependencyInjection;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic;
using PravoRu.DataLake.Common;

namespace PravoRu.DataLake.ArbitrageLoad.Etl
{
	/// <summary>
	/// Класс конфигуратор
	/// </summary>
	public class EtlConfig : IServiceConfig
	{
		/// <summary>
		/// Метод конфигурирования
		/// </summary>
		/// <param name="collection"></param>
		public void Config(IServiceCollection collection)
		{
			collection
				.AddSingleton<IStatWorker, AvgStatWorker>()
				.AddSingleton<IStatWorker, ClaimStatWorker>()
				.AddSingleton<IElasticSettings,ElasticSettings>()
				.AddSingleton<IEtlSettings, EtlSettings>();
		}
	}
}