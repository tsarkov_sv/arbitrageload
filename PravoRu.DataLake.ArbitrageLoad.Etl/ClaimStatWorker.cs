﻿using System;
using NLog;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Model;
using Nest;
using System.Collections.Generic;
using PravoRu.DataLake.ArbitrageLoad.DataAccess.Model;
using System.Linq;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic;
using PravoRu.DataLake.ArbitrageLoad.DataAccess;

namespace PravoRu.DataLake.ArbitrageLoad.Etl
{
	/// <summary>
	/// Класс сбора исковой нагрузки по отрасли, региону
	/// </summary>
	public class ClaimStatWorker : StatWorkerBase
	{
		private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();
		
		/// <summary>
		/// .ctor
		/// </summary>
		public ClaimStatWorker(IArbitrageContextApi context, IEtlSettings etlSettings, IElasticSettings elasticSettings) 
			: base(context, etlSettings, elasticSettings ,Logger)
		{
		}

		/// <inheritdoc/>
		public override void Run()
		{
			logger.Info($"Начало сбора статистики {nameof(AvgStatWorker)}");
			StartDate = etlSettings.GetStartDateLoad();
			StartDate = new DateTime(StartDate.Year, StartDate.Month, 1);

			logger.Info($"Дата начала сбора статистики: {StartDate}");
			var counter = 1;
			var months = Math.Abs((DateTime.Now.Month - StartDate.Month) + 12 * (DateTime.Now.Year - StartDate.Year));
			while (StartDate <= DateTime.Now)
			{
				foreach (var region in Regions)
				{
					List<ClaimStatistics> claimStatistics = new List<ClaimStatistics>();
					foreach (var okved in Okveds)
					{
						var response = elasticClient.Search<Side>(s => s.Size(0).Aggregations(a =>
						a.Average("claims_avg", aa => aa.Field(aaf => aaf.ClaimSum))
						.Sum("claims_sum", asum => asum.Field(asf => asf.ClaimSum))
						.Percentiles("claims_percentiles", ap => ap.Field(apf => apf.ClaimSum).Percents(new double[] { 50, 75, 90, 95, 99 })))
						.Query(
								q => q.Bool(
									b => b.MustNot(
											GetNotMust(2, 6)//Не должны попадать банкротные дела(2 банкротное ЮЛ, 6 БФЛ)
										)
										.Must(
											GetMust(1, region, okved) // Фильтрация по ответчику, региону и отрасли
										)
								)
							));
						var percentiles = response.Aggs.Percentiles("claims_percentiles")?.Items ?? default(IList<PercentileItem>);
						ClaimStatistics claimStatistic = new ClaimStatistics()
						{
							Date = StartDate,
							AvgClaims = response.Aggs.Average("claims_avg")?.Value ?? 0,
							OkvedCode = okved,
							RegionCode = region,
							SumClaims = response.Aggs.Sum("claims_sum").Value ?? 0,
							Percentile50 = percentiles.FirstOrDefault(a => a.Percentile == 50)?.Value ?? 0,
							Percentile75 = percentiles.FirstOrDefault(a => a.Percentile == 75)?.Value ?? 0,
							Percentile90 = percentiles.FirstOrDefault(a => a.Percentile == 90)?.Value ?? 0,
							Percentile95 = percentiles.FirstOrDefault(a => a.Percentile == 95)?.Value ?? 0,
							Percentile99 = percentiles.FirstOrDefault(a => a.Percentile == 99)?.Value ?? 0,
						};
						claimStatistics.Add(claimStatistic);
					}
					context.RemoveRangeClaimStat(StartDate, region);
					context.AddRangeClaimStat(claimStatistics);
					logger.Info($"Сохранена статистика {nameof(ClaimStatWorker)} для региона:{region}");
					logger.Info($"Процент сбора статистики: {counter * 100 / (Regions.Count * Okveds.Count * months)}%");
					counter++;
				}
				StartDate.AddMonths(1);
			}
		}

		protected override QueryContainer[] GetMust(int side, int? region, int okved)
		{
			var queryContainers = new List<QueryContainer>(base.GetMust(side, region, okved));

			queryContainers.Add(new MatchQuery()
			{
				Field = new Field("claimSumActiveMonthsForAggs"),
				Query = StartDate.ToString("yyyy-MM-dd")
			});
			return queryContainers.ToArray();
		}
	}
}