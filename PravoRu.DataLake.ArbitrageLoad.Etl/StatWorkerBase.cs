﻿using System;
using System.Collections.Generic;
using Nest;
using NLog;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic;
using PravoRu.DataLake.ArbitrageLoad.DataAccess;

namespace PravoRu.DataLake.ArbitrageLoad.Etl
{
	/// <summary>
	/// Базовый класс сбора статистики
	/// </summary>
	public abstract class StatWorkerBase : IStatWorker
	{
		protected IArbitrageContextApi context; 
		protected IServiceProvider serviceProvider;
		protected ILogger logger;
		protected IElasticSettings elasticSettings;
		protected IElasticClient elasticClient;
		protected IEtlSettings etlSettings;
		
		/// <summary>
		/// Список окведов по которым будет строиться запрос
		/// </summary>
		public List<int> Okveds { get; set; }

		/// <summary>
		/// Список регионов ко которым будет строиться запрос
		/// </summary>
		public List<int?> Regions { get; set; }

		/// <summary>
		/// Дата начала периода
		/// </summary>
		public DateTime StartDate { get; set; }

		/// <summary>
		/// Список сторон
		/// </summary>
		public List<int> Sides { get; set; }

		/// <summary>
		/// .ctor
		/// </summary>
		/// <param name="serviceProvider"></param>
		/// <param name="logger"></param>
		public StatWorkerBase(IArbitrageContextApi context, IEtlSettings etlSettings, IElasticSettings elasticSettings, ILogger logger)
		{
			this.logger = logger;
			this.context = context;
			this.etlSettings = etlSettings;
			this.elasticSettings = elasticSettings;
			elasticClient = elasticSettings.GetElasticCaseClient();

			Okveds = this.context.GetOkveds();
			Regions = this.context.GetRegions();
			Sides = this.context.GetSides();
		}
		
		/// <inheritdoc/>
		public abstract void Run();

		protected virtual QueryContainer[] GetMust(int side, int? region, int okved)
		{
			string okvedStr = okved < 10 ? $"0{okved}" : $"{okved}";
			var queryContainer = new List<QueryContainer>();
			var regionQuery = new MatchQuery()
			{
				Field = new Field("nRegion"),
				Query = $@"{region}"
			};
			var sideQuery = new MatchQuery()
			{
				Field = new Field("sideType"),
				Query = $@"{side}"
			};
			var okvedQuery = new MatchQuery()
			{
				Field = new Field("mainOkved"),
				Query = $@"K{okvedStr}"
			};
			
			if (side == 0 || side == 1)
			{
				queryContainer.AddRange(new List<QueryContainer>() { regionQuery, sideQuery, okvedQuery });
			}
			else
			{
				queryContainer.AddRange(new List<QueryContainer>() { regionQuery, okvedQuery });
			}
			return queryContainer.ToArray();
		}

		protected virtual QueryContainer[] GetNotMust(int side)
		{
			var queryContainer = new List<QueryContainer>();
			var termQuery = new TermQuery()
			{
				Field = new Field("nOgrn"),
				Value = "OGRN"
			};
			var side0Term = new TermQuery()
			{
				Field = new Field("sideType"),
				Value = "0"
			};
			var side1Term = new TermQuery()
			{
				Field = new Field("sideType"),
				Value = "1"
			};
			if (side == 0 || side == 1)
			{
				queryContainer.Add(termQuery);
			}
			else
			{
				queryContainer.AddRange(new List<QueryContainer>() { termQuery, side0Term, side1Term });
			}
			return queryContainer.ToArray();
		}

		protected virtual QueryContainer[] GetNotMust(params int[] sides)
		{
			var queryContainer = new List<QueryContainer>();
			queryContainer.Add(new TermQuery()
			{
				Field = new Field("nOgrn"),
				Value = "OGRN"
			});
			if (sides != null)
			{
				foreach (var item in sides)
				{
					queryContainer.Add(new TermQuery()
					{
						Field = new Field("sideType"),
						Value = item
					});
				}
			}
			return queryContainer.ToArray();
		}
	}
}