﻿using Nest;
using System;

namespace PravoRu.DataLake.ArbitrageLoad.Etl
{
	/// <summary>
	/// Класс настроек
	/// </summary>
	public interface IEtlSettings
	{
		/// <summary>
		/// Получение даты с которой необходимо собрать статистику
		/// </summary>
		/// <returns></returns>
		DateTime GetStartDateLoad();

		/// <summary>
		/// Сохранение даты последнего сбора статистики
		/// </summary>
		/// <param name="date"></param>
		void SaveStartDateLoad(DateTime date);
	}
}
