﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PravoRu.DataLake.ArbitrageLoad.Etl
{
	/// <summary>
	/// Класс сборщика статистики
	/// </summary>
	public interface IStatWorker
	{
		/// <summary>
		/// Метод запуска сбора статистики
		/// </summary>
		void Run();
	}
}