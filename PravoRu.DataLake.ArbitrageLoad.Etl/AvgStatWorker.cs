﻿using Nest;
using NLog;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Model;
using PravoRu.DataLake.ArbitrageLoad.DataAccess;
using PravoRu.DataLake.ArbitrageLoad.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using ArbitrageStatistics = PravoRu.DataLake.ArbitrageLoad.DataAccess.Model.ArbitrageStatistics;

namespace PravoRu.DataLake.ArbitrageLoad.Etl
{
	/// <summary>
	/// Класс сборщика статистики
	/// </summary>
	public class AvgStatWorker : StatWorkerBase
	{
		private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();
		
		/// <summary>
		/// .ctor
		/// </summary>
		public AvgStatWorker(IArbitrageContextApi context, IEtlSettings etlSettings, IElasticSettings elasticSettings) 
			: base(context, etlSettings, elasticSettings, Logger)
		{
		}

		/// <summary>
		/// Метод запуска сбора статистики
		/// </summary>
		public override void Run()
		{
			logger.Info($"Начало сбора статистики {nameof(AvgStatWorker)}");
			StartDate = etlSettings.GetStartDateLoad();
			StartDate = new DateTime(StartDate.Year, StartDate.Month, 1);
			logger.Info($"Дата начала сбора статистики: {StartDate}");
			var counter = 1;
			foreach (var side in Sides)
			{
				foreach (var region in Regions)
				{
					foreach (var okved in Okveds)
					{
						List<ArbitrageStatistics> arbitrageList = new List<ArbitrageStatistics>();
						var searchResponse = elasticClient.Search<Side>(s =>
							s.Query(
									q => q.Bool(
										b => b.MustNot(
												GetNotMust(side)
											)
											.Must(
												GetMust(side, region, okved)
											)
									)
								)
								.Aggregations(a => a.DateHistogram("by_registration_date",
										dh => dh.Field(new Field("sideRegistrationDate"))
											.Interval(DateInterval.Month)
											.Format("yyyy-MM")
											.Aggregations(aa => aa
												.Terms("ogrns",
													t => t.Field(new Field("nOgrn"))
														.Size(0)
														.Aggregations(aaa => aaa
															.ValueCount("count_value", v => v.Field(new Field("nOgrn")))
														)
												)
												.AverageBucket("avg_monthly_ogrn",
													av => av.BucketsPath("ogrns>count_value"))
												.PercentilesBucket("percentiles_monthly_ogrns",
													pb => pb.BucketsPath("ogrns>count_value")
														.Percents(new double[] {50, 75, 90, 95, 99}))
											)
									)
								)
						);
						var aggs = (BucketAggregate) searchResponse.Aggregations["by_registration_date"];
						foreach (DateHistogramBucket bucket in aggs.Items)
						{
							var avg = ((ValueAggregate) bucket?.Aggregations["avg_monthly_ogrn"])?.Value;
							var percentil = ((PercentilesAggregate) bucket?.Aggregations["percentiles_monthly_ogrns"])?.Items ?? default(IList<PercentileItem>);
							ArbitrageStatistics arbitrageStatistics = new ArbitrageStatistics();
							arbitrageStatistics.Date = bucket.Date;
							arbitrageStatistics.AvgCountCase = (long) (avg ?? 0);
							arbitrageStatistics.OkvedCode = okved;
							arbitrageStatistics.Percentile50 = percentil.FirstOrDefault(a => a.Percentile == 50)?.Value ?? 0;
							arbitrageStatistics.Percentile75 = percentil.FirstOrDefault(a => a.Percentile == 75)?.Value ?? 0;
							arbitrageStatistics.Percentile90 = percentil.FirstOrDefault(a => a.Percentile == 90)?.Value ?? 0;
							arbitrageStatistics.Percentile95 = percentil.FirstOrDefault(a => a.Percentile == 95)?.Value ?? 0;
							arbitrageStatistics.Percentile99 = percentil.FirstOrDefault(a => a.Percentile == 99)?.Value ?? 0;
							arbitrageStatistics.RegionCode = region;
							arbitrageStatistics.SideType = (TypeSide) side;
							arbitrageList.Add(arbitrageStatistics);
						}

						var date = new DateTime(StartDate.Year, StartDate.Month, 1);
						context.RemoveRangeArbitrStat(date, okved, region, side);
						context.AddRangeArbitrStat(arbitrageList);
						logger.Info($"Сохранена статистика для Okved:{okved} Region:{region}");
						logger.Info(
							$"Процент сбора статистики: {counter * 100 / (Regions.Count * Okveds.Count * Sides.Count)}%");
						counter++;
					}
				}
			}
			etlSettings.SaveStartDateLoad(DateTime.Now);
		}

		protected override QueryContainer[] GetMust(int side, int? region, int okved)
		{
			var queryContainers = new List<QueryContainer>(base.GetMust(side, region, okved));

			queryContainers.Add(new DateRangeQuery()
			{
				Field = new Field("sideRegistrationDate"),
				GreaterThanOrEqualTo = StartDate,
				LessThanOrEqualTo = DateTime.Now
			});
			return queryContainers.ToArray();
		}
	}
}