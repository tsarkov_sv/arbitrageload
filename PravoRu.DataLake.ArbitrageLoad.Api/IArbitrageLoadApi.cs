﻿using PravoRu.DataLake.ArbitrageLoad.Api.Model;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Model;
using PravoRu.DataLake.ArbitrageLoad.DataAccess.Model;
using System;
using System.Collections.Generic;
using ArbitrageStatistics = PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Model.ArbitrageStatistics;

namespace PravoRu.DataLake.ArbitrageLoad.Api
{
	/// <summary>
	/// Интерфейс библиотеки API
	/// </summary>
	public interface IArbitrageLoadApi
	{
		/// <summary>
		/// Метод получения арбитражной статистики
		/// </summary>
		/// <param name="request">Модель запроса статистики</param>
		/// <returns></returns>
		List<ArbitrageStatistics> GetArbitrageStatistic(ArbitrageStatRequest request);

		/// <summary>
		/// Метод получения признака частоты участия компании в судебных делах
		/// </summary>
		/// <param name="info">Реквизиты организации</param>
		/// <returns>Признак частоты участия</returns>
		FrequencyEnum GetFrequencyAttribute(OrganizationInfo info);

		/// <summary>
		/// Метод получения статистики по исковой нагрузке
		/// </summary>
		/// <param name="request">Модель запроса статистики</param>
		/// <returns></returns>
		List<ClaimsStatistics> GetClaimStatistic(ClaimStatRequest request);
	}
}