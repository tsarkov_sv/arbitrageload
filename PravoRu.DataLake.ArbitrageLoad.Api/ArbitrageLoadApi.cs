﻿using System;
using PravoRu.DataLake.ArbitrageLoad.DataAccess;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Model;
using System.Linq;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Mapper;
using System.Collections.Generic;
using PravoRu.DataLake.ArbitrageLoad.Api.Model;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic;
using Nest;
using PravoRu.Common.WebApi.Exceptions;
using System.Text.RegularExpressions;

namespace PravoRu.DataLake.ArbitrageLoad.Api
{
	/// <summary>
	/// Класс методов API
	/// </summary>
	public class ArbitrageLoadApi : IArbitrageLoadApi
	{
		private IServiceProvider _provider;

		/// <summary>
		/// .ctor
		/// </summary>
		/// <param name="provider"></param>
		public ArbitrageLoadApi(IServiceProvider provider)
		{
			_provider = provider;
		}

		/// <summary>
		/// Метод получения признака частоты участия компании в судебных делах
		/// </summary>
		/// <param name="info">Реквизиты организации</param>
		/// <returns>Признак частоты участия</returns>
		public FrequencyEnum GetFrequencyAttribute(OrganizationInfo info)
		{
			//Получаем настройки elastic search и клиенты к индексам
			var elasticSettings = (IElasticSettings) _provider.GetService(typeof(IElasticSettings));
			var casesClient = elasticSettings.GetElasticCaseClient();
			var orgsClient = elasticSettings.GetElasticOrgClient();
			//Получаем дату регистрации организации
			var response = GetDateRegistrationOrganization(orgsClient, info);
			var dateRegistration = response.Item1;
			var okved = response.Item2;
			if (dateRegistration == null || okved == null)
			{
				throw new BadRequestException(
					"По заданным реквизитам не найдена организация или не удалось получить ОКВЭД");
			}
			DateTime? lastCaseRegDate;
			//Получаем количество дел организации и дату последнего зарегистрированного дела
			var countCases = GetCaseCountOrganization(casesClient, info, out lastCaseRegDate);
			if (!countCases.HasValue || countCases == 0)
			{
				return FrequencyEnum.Never;
			}
			var age = Math.Round((lastCaseRegDate.Value - dateRegistration.Value).TotalDays / 365 * 12) + 1;
			//Вычисляем коэффициент частоты участия данной организации
			var frequency = countCases / age;
			var context = (IArbitrageContextApi) _provider.GetService(typeof(IArbitrageContextApi));
			//Запрос из бд предрасчитанных значений
			var result = context.GetFrequencyBordersByOkved(okved.Value);
			if (result == null)
			{
				throw new BadRequestException("Границы частоты участия в делах не найдены по заданному ОКВЭД");
			}
			//если в диапазоне, то редко, больше - часто, меньше иногда
			return frequency >= result.LeftBorder && frequency <= result.RightBorder
				? FrequencyEnum.Rarely
				: frequency > result.RightBorder
					? FrequencyEnum.Often
					: FrequencyEnum.Sometimes;
		}

		/// <summary>
		/// Метод получения статистики по исковой нагрузке
		/// </summary>
		/// <param name="request">Модель запроса статистики</param>
		/// <returns></returns>
		public List<ClaimsStatistics> GetClaimStatistic(ClaimStatRequest request)
		{
			List<ClaimsStatistics> response = new List<ClaimsStatistics>();
			var context = (IArbitrageContextApi) _provider.GetService(typeof(IArbitrageContextApi));
			var listArbitrStat =
				context.GetClaimStatisticses(request.StartDate, request.EndDate, request.Okved, request.Region);

			if (listArbitrStat.Count > 0)
			{
				var mapperFactory = (IEtlMapperFactory) _provider.GetService(typeof(IEtlMapperFactory));
				var mapper = mapperFactory.Create();
				foreach (var stat in listArbitrStat)
				{
					response.Add(mapper.Map<ClaimsStatistics>(stat));
				}
				return response;
			}
			else
			{
				throw new NoContentException("Статистика по заданным фильтрам не найдена");
			}
		}

		/// <summary>
		/// Метод получения статистики
		/// </summary>
		/// <param name="request">Модель запроса статистики</param>
		/// <returns></returns>
		public List<ArbitrageStatistics> GetArbitrageStatistic(ArbitrageStatRequest request)
		{
			List<ArbitrageStatistics> response = new List<ArbitrageStatistics>();
			var context = (IArbitrageContextApi) _provider.GetService(typeof(IArbitrageContextApi));
			var listArbitrStat =
				context.GetArbitrageStatisticses(request.StartDate, request.EndDate, request.Okved, request.Region);

			if (listArbitrStat.Count > 0)
			{
				if (request.TypeSide != null)
				{
					listArbitrStat = listArbitrStat.FindAll(a => a.SideType == request.TypeSide);
				}
				var mapperFactory = (IEtlMapperFactory) _provider.GetService(typeof(IEtlMapperFactory));
				var mapper = mapperFactory.Create();
				foreach (var stat in listArbitrStat)
				{
					response.Add(mapper.Map<ArbitrageStatistics>(stat));
				}
				return response;
			}
			else
			{
				throw new NoContentException("Статистика по заданным фильтрам не найдена");
			}
		}

		private Tuple<DateTime?, int?> GetDateRegistrationOrganization(IElasticClient client, OrganizationInfo info)
		{
			var queryContainer = GetQueryContainer(info);
			queryContainer.Add(new MatchQuery()
			{
				Field = new Field("isBranch"),
				Query = "false"
			});
			var response = client.Search<FnsOrganization>(s =>
				s.Size(1).Query(q =>
					q.Bool(b =>
						b.Must(queryContainer.ToArray())
					)
				)
			);
			int? okved = null;
			var mainOkved = response.Documents?.FirstOrDefault()?.Industry?.Code;
			if (mainOkved != null)
			{
				int intOkved;
				if (int.TryParse(Regex.Match(mainOkved, @"^\d{0,2}").Value, out intOkved))
				{
					okved = intOkved;
				}
			}
			return new Tuple<DateTime?, int?>(response.Documents?.FirstOrDefault()?.RegistrationDate, okved);
		}

		private double? GetCaseCountOrganization(IElasticClient client, OrganizationInfo info,
			out DateTime? lastCaseRegDate)
		{
			var queryContainer = GetQueryContainer(info, "sides.", "INN", "OGRN");
			var response = client.Search<Case>(s =>
				s.Size(1).Query(q =>
						q.Nested(n => n.Query(nq =>
								nq.Bool(b =>
									b.Must(queryContainer.ToArray())
								)).Path(new Field("sides"))
						)
					)
					.Sort(st => st.Descending(new Field("registrationDate")))
			);
			lastCaseRegDate = response.Documents.FirstOrDefault()?.RegistrationDate;
			return response.Total;
		}

		private List<QueryContainer> GetQueryContainer(OrganizationInfo info, string nestedPath = null,
			string prefixinn = null, string prefixogrn = null)
		{
			var queryContainer = new List<QueryContainer>();
			if (PravoRu.Common.Requisites.RequisitesHelper.IsInnValid(info.Inn))
			{
				queryContainer.Add(new MatchQuery()
				{
					Field = new Field(String.Format("{0}{1}", $"{nestedPath}",
						!String.IsNullOrWhiteSpace(prefixinn) ? "nInn" : "inn")),
					Query = $"{prefixinn}{info.Inn}"
				});
			}
			if (PravoRu.Common.Requisites.RequisitesHelper.IsOgrnValid(info.Ogrn))
			{
				queryContainer.Add(new MatchQuery()
				{
					Field = new Field(String.Format("{0}{1}", $"{nestedPath}",
						!String.IsNullOrWhiteSpace(prefixogrn) ? "nOgrn" : "ogrn")),
					Query = $"{prefixogrn}{info.Ogrn}"
				});
			}
			if (PravoRu.Common.Requisites.RequisitesHelper.IsOkpoValid(info.Okpo) &&
			    String.IsNullOrWhiteSpace(prefixogrn))
			{
				queryContainer.Add(new MatchQuery()
				{
					Field = new Field("okpo"),
					Query = $"{info.Okpo}"
				});
			}
			return queryContainer;
		}
	}
}