﻿using PravoRu.Common.Requisites;
using System;
using System.Collections.Generic;
using System.Text;

namespace PravoRu.DataLake.ArbitrageLoad.Api.Model
{
	/// <summary>
	/// Модель запроса частоты участия в судебных делах
	/// </summary>
	public class FrequencyRequest
	{
		/// <summary>
		/// Реквизиты компании
		/// </summary>
		public OrganizationInfo OrgInfo { get; set; }

		/// <summary>
		/// Метод валидации реквизитов
		/// </summary>
		/// <returns></returns>
		public bool Validate()
		{
			if (OrgInfo != null && !OrgInfo.IsEmpty())
			{
				if (OrgInfo.Inn != null && !RequisitesHelper.IsInnValid(OrgInfo.Inn))
				{
					return false;
				}
				if (OrgInfo.Ogrn != null && !RequisitesHelper.IsOgrnValid(OrgInfo.Ogrn))
				{
					return false;
				}
				if (!String.IsNullOrWhiteSpace(OrgInfo.Okpo) && !RequisitesHelper.IsOkpoValid(OrgInfo.Okpo))
				{
					return false;
				}
				return true;
			}
			return false;
		}
	}
}