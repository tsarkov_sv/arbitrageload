﻿using PravoRu.DataLake.ArbitrageLoad.DataAccess.Model;
using System;

namespace PravoRu.DataLake.ArbitrageLoad.Api.Model
{
	/// <summary>
	/// Запрос для получения статистики
	/// </summary>
	public class ArbitrageStatRequest : BaseRequest
	{
		/// <summary>
		/// Тип участника дела
		/// </summary>
		public TypeSide? TypeSide { get; set; }
	}
}