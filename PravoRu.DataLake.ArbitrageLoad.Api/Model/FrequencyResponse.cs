﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PravoRu.DataLake.ArbitrageLoad.Api.Model
{
	/// <summary>
	/// Модель ответа признака частоты участия в судебных делах
	/// </summary>
	public class FrequencyResponse
	{
		/// <summary>
		/// Признак частоты участия в судебных делах
		/// </summary>
		public FrequencyEnum Frequency { get; set; }
	}
}