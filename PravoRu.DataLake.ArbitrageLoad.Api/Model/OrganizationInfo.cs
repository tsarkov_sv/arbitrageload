﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PravoRu.DataLake.ArbitrageLoad.Api.Model
{
	/// <summary>
	/// Модель реквизитов компании
	/// </summary>
	public class OrganizationInfo
	{
		/// <summary>
		/// ИНН
		/// </summary>
		public string Inn { get; set; }

		/// <summary>
		/// ОГРН
		/// </summary>
		public string Ogrn { get; set; }

		/// <summary>
		/// ОКПО
		/// </summary>
		public string Okpo { get; set; }

		/// <summary>
		/// Метод проверки заполненых значений
		/// </summary>
		/// <returns></returns>
		public bool IsEmpty()
		{
			return String.IsNullOrWhiteSpace(Inn) && String.IsNullOrWhiteSpace(Ogrn) && String.IsNullOrWhiteSpace(Okpo);
		}
	}
}