﻿using PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Model;
using System.Collections.Generic;

namespace PravoRu.DataLake.ArbitrageLoad.Api.Model
{
	/// <summary>
	/// Модель ответа со статистикой
	/// </summary>
	public class StatResponse<T>
	{
		/// <summary>
		/// Список данных статистики
		/// </summary>
		public List<T> Statistics { get; set; }
	}
}