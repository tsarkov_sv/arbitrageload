﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PravoRu.DataLake.ArbitrageLoad.Api.Model
{
	/// <summary>
	/// Перечисление признаков
	/// </summary>
	public enum FrequencyEnum
	{
		/// <summary>
		/// Никогда
		/// </summary>
		Never = 0,

		/// <summary>
		/// Иногда
		/// </summary>
		Sometimes = 1,

		/// <summary>
		/// Редко
		/// </summary>
		Rarely = 2,

		/// <summary>
		/// Часто
		/// </summary>
		Often = 3
	}
}