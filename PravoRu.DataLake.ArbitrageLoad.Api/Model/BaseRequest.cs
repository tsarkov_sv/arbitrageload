﻿using System;

namespace PravoRu.DataLake.ArbitrageLoad.Api.Model
{
	/// <summary>
	/// Базовый класс запроса
	/// </summary>
	public class BaseRequest
	{
		/// <summary>
		/// ОКВЭД
		/// </summary>
		public int Okved { get; set; }

		/// <summary>
		/// Номер региона
		/// </summary>
		public int? Region { get; set; }

		/// <summary>
		/// Дата начала периода статистики
		/// </summary>
		public DateTime? StartDate { get; set; } = DateTime.Now.AddMonths(-6);

		/// <summary>
		/// Дата завершния периода статистики
		/// </summary>
		public DateTime? EndDate { get; set; } = DateTime.Now;

		internal bool IsValid
		{
			get { return Okved > 0 && Okved <= 99 && (Region != null ? Region > 0 && Region < 96 : true); }
		}

		/// <summary>
		/// Метод валидации объекта
		/// </summary>
		/// <returns></returns>
		public bool Validate()
		{
			return IsValid;
		}
	}
}