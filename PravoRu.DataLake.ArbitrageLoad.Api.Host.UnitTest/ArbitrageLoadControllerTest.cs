using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using PravoRu.DataLake.ArbitrageLoad.Api.Host.Controllers.v1;
using PravoRu.DataLake.ArbitrageLoad.Api.Model;
using PravoRu.DataLake.ArbitrageLoad.DataAccess.Model;
using Shouldly;

namespace PravoRu.DataLake.ArbitrageLoad.Api.Host.UnitTest
{
    [TestFixture]
    public class ArbitrageLoadControllerTest
    {
        [TestCase(0, 10)]
        [TestCase(100, 10)]
        [TestCase(10, 0)]
        [TestCase(10, 96)]
        public void StatByOkvedRegion_BadRequest(int okved, int? region)
        {
            var controller = new ArbitrageLoadController(null);
            var response = controller.StatArbitrageByOkvedRegion(new ArbitrageStatRequest()
            {
				Okved = okved,
				Region = region,
				EndDate = null,
                StartDate = null,
                TypeSide = TypeSide.Defendants
            });
            (response as BadRequestObjectResult).ShouldNotBeNull();
            (response as BadRequestObjectResult).Value.ShouldBe("Модель запроса статистики не прошла валидацию.");
        }
	    [TestCase(null,null,null)]
	    [TestCase(1,null,null)]
	    [TestCase(7705041231,1,null)]
	    [TestCase(1,1027739068060,null)]
	    [TestCase(7705041231,1027739068060,1)]
	    public void GetFrequencyAttribute_BadRequest(long? inn, long? ogrn, long? okpo)
	    {
		    var controller = new ArbitrageLoadController(null);
		    var response=controller.GetFrequencyAttribute(new FrequencyRequest()
		    {
			    OrgInfo = new OrganizationInfo()
			    {
				    Inn = inn?.ToString(),
				    Ogrn = ogrn?.ToString(),
				    Okpo = okpo?.ToString()
			    }
		    });
		    (response as BadRequestObjectResult).ShouldNotBeNull();
		    (response as BadRequestObjectResult).Value.ShouldBe("Модель запроса признака частоты участия компании в судебных делах не прошла валидацию.");
	    }

		[TestCase(0, 10)]
		[TestCase(100, 10)]
		[TestCase(10, 0)]
		[TestCase(10, 96)]
		public void ClaimStatByOkvedRegion_BadRequest(int okved, int? region)
		{
			var controller = new ArbitrageLoadController(null);
			var response = controller.StatClaimByOkvedRegion(new ClaimStatRequest()
			{
				Okved= okved,
				Region = region,
				EndDate = null,
				StartDate = null
			});
			(response as BadRequestObjectResult).ShouldNotBeNull();
			(response as BadRequestObjectResult).Value.ShouldBe("Модель запроса статистики не прошла валидацию.");
		}

	}
}