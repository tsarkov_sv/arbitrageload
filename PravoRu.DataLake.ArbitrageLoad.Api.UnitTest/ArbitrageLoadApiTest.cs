using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nest;
using NUnit.Framework;
using PravoRu.Common.WebApi.Exceptions;
using PravoRu.DataLake.ArbitrageLoad.Api.Model;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Mapper;
using PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Model;
using PravoRu.DataLake.ArbitrageLoad.DataAccess;
using PravoRu.DataLake.ArbitrageLoad.DataAccess.Model;
using Shouldly;
using ArbitrageStatistics = PravoRu.DataLake.ArbitrageLoad.DataAccess.Model.ArbitrageStatistics;
using Case = PravoRu.DataLake.ArbitrageLoad.BusinessLogic.Model.Case;

namespace PravoRu.DataLake.ArbitrageLoad.Api.UnitTest
{
	[TestFixture]
	public class ArbitrageLoadApiTest
	{
		private IElasticSettings _elasticSettings;
		private IDataSettings _settings;
		private Mock<IServiceProvider> _provider;
		private IElasticClient _clientOrganization;
		private IElasticClient _clientCases;

		[NUnit.Framework.SetUp]
		public void SetUp()
		{
			var searchResponseCase = new Mock<ISearchResponse<Case>>();
			searchResponseCase.Setup(x => x.Documents)
				.Returns(new List<Case> { new Case { RegistrationDate = DateTime.Now.AddYears(-10) } });
			searchResponseCase.Setup(x => x.Total).Returns(1);
			var mockClientCase = new Mock<IElasticClient>();
			mockClientCase
				.Setup(client => client.Search<Case>(It.IsAny<Func<SearchDescriptor<Case>, ISearchRequest>>()))
				.Returns(searchResponseCase.Object);
			_clientCases = mockClientCase.Object;
			var mockElasticSettings = new Mock<IElasticSettings>();
			mockElasticSettings.Setup(elSettings => elSettings.GetElasticCaseClient()).Returns(_clientCases);
			var searchResponseOrg = new Mock<ISearchResponse<FnsOrganization>>();
			searchResponseOrg.Setup(x => x.Documents)
				.Returns(new List<FnsOrganization>
				{
					new FnsOrganization
					{
						Industry = new Industry()
						{
							Code = "65.13",
							Name = "�������� �������"
						},
						RegistrationDate = DateTime.Now.AddYears(-10)
					}
				});
			searchResponseOrg.Setup(x => x.Total).Returns(1);
			var mockClientOrg = new Mock<IElasticClient>();
			mockClientOrg
				.Setup(client =>
					client.Search<FnsOrganization>(It.IsAny<Func<SearchDescriptor<FnsOrganization>, ISearchRequest>>()))
				.Returns(searchResponseOrg.Object);
			_clientOrganization = mockClientOrg.Object;
			mockElasticSettings.Setup(elSettings => elSettings.GetElasticOrgClient()).Returns(_clientOrganization);
			_elasticSettings = mockElasticSettings.Object;
			var mockSettings = new Mock<IDataSettings>();
			mockSettings.Setup(settings => settings.GetConnectionString()).Returns("");
			_settings = mockSettings.Object;

			var mockServiceProvider = new Mock<IServiceProvider>();
			mockServiceProvider.Setup(service => service.GetService(typeof(IElasticSettings)))
				.Returns(_elasticSettings);

			mockServiceProvider.Setup(s => s.GetService(typeof(IEtlMapperFactory))).Returns(new EtlMapperFactory());
			_provider = mockServiceProvider;
		}

		[TestCase(0.01, 0.02, FrequencyEnum.Often)]
		[TestCase(1, 2, FrequencyEnum.Rarely)]
		[TestCase(0.1, 1, FrequencyEnum.Rarely)]
		[TestCase(1.1, 3, FrequencyEnum.Sometimes)]
		public void GetFrequencyAttribute(double left, double right, FrequencyEnum ExpectedResult)
		{
			var arbitrageLoadContext = new Mock<IArbitrageContextApi>();
			arbitrageLoadContext.Setup(context =>
				context.GetFrequencyBordersByOkved(It.IsAny<int>())).Returns(
				() =>
				{
					return new FrequencyBorders()
					{
						Id = 1,
						LeftBorder = left,
						Okved = 65,
						RightBorder = right
					};
				});
			_provider.Setup(service => service.GetService(typeof(IArbitrageContextApi)))
				.Returns(arbitrageLoadContext.Object);
			var api = new ArbitrageLoadApi(_provider.Object);
			var result = api.GetFrequencyAttribute(new OrganizationInfo()
			{
				Inn = "inn",
				Ogrn = "ogrn",
				Okpo = "okpo"
			});
			result.ShouldBe(ExpectedResult);
		}

		[TestCase(TypeSide.Plaintiffs, 2)]
		[TestCase(TypeSide.Third, 1)]
		[TestCase(TypeSide.Defendants, 3)]
		public void GetArbitrageStatisticses(TypeSide typeSide, int count)
		{
			var arbitrageLoadContext = new Mock<IArbitrageContextApi>();
			arbitrageLoadContext.Setup(context => context.GetArbitrageStatisticses(It.IsAny<DateTime?>(),
					It.IsAny<DateTime?>(),
					It.IsAny<int>(),
					It.IsAny<int?>()))
				.Returns(() => new List<ArbitrageStatistics>()
				{
					new ArbitrageStatistics()
					{
						SideType = TypeSide.Plaintiffs
					},
					new ArbitrageStatistics()
					{
						SideType = TypeSide.Plaintiffs
					},
					new ArbitrageStatistics()
					{
						SideType = TypeSide.Third
					},
					new ArbitrageStatistics()
					{
						SideType = TypeSide.Defendants
					},
					new ArbitrageStatistics()
					{
						SideType = TypeSide.Defendants
					},
					new ArbitrageStatistics()
					{
						SideType = TypeSide.Defendants
					}
				});
			_provider.Setup(p => p.GetService(typeof(IArbitrageContextApi))).Returns(arbitrageLoadContext.Object);
			var api = new ArbitrageLoadApi(_provider.Object);
			var list = api.GetArbitrageStatistic(new ArbitrageStatRequest()
			{
				StartDate = null,
				EndDate = null,
				Okved = 01,
				Region = null,
				TypeSide = typeSide
			});
			list.Count().ShouldBe(count);
		}

		[Test]
		public void GetArbitrageStatisticses_ThrowNoContentException()
		{
			var arbitrageLoadContext = new Mock<IArbitrageContextApi>();
			arbitrageLoadContext.Setup(context => context.GetArbitrageStatisticses(It.IsAny<DateTime?>(),
					It.IsAny<DateTime?>(),
					It.IsAny<int>(),
					It.IsAny<int?>()))
				.Returns(() => new List<ArbitrageStatistics>());
			_provider.Setup(p => p.GetService(typeof(IArbitrageContextApi))).Returns(arbitrageLoadContext.Object);
			var api = new ArbitrageLoadApi(_provider.Object);
			Should.Throw<NoContentException>(() =>
			{
				api.GetArbitrageStatistic(new ArbitrageStatRequest()
				{
					StartDate = null,
					EndDate = null,
					Okved = 01,
					Region = null,
					TypeSide = TypeSide.Defendants
				});
			});
		}

		[Test]
		public void GetClaimStatistic_ThrowNoContentException()
		{
			var arbitrageLoadContext = new Mock<IArbitrageContextApi>();
			arbitrageLoadContext.Setup(context => context.GetClaimStatisticses(It.IsAny<DateTime?>(),
					It.IsAny<DateTime?>(),
					It.IsAny<int>(),
					It.IsAny<int?>()))
				.Returns(() => new List<ClaimStatistics>());
			_provider.Setup(p => p.GetService(typeof(IArbitrageContextApi))).Returns(arbitrageLoadContext.Object);
			var api = new ArbitrageLoadApi(_provider.Object);
			Should.Throw<NoContentException>(() =>
			{
				api.GetClaimStatistic(new ClaimStatRequest()
				{
					StartDate = null,
					EndDate = null,
					Okved = 01,
					Region = null
				});
			});
		}


		[TestCase(56, 63)]
		public void GetClaimStatistic(int okved, int region)
		{
			var arbitrageLoadContext = new Mock<IArbitrageContextApi>();
			arbitrageLoadContext.Setup(context => context.GetClaimStatisticses(It.IsAny<DateTime?>(),
					It.IsAny<DateTime?>(),
					It.IsAny<int>(),
					It.IsAny<int?>()))
				.Returns(() => new List<ClaimStatistics>()
				{
					new ClaimStatistics()
					{
						OkvedCode = okved,
						RegionCode = region
					}
				});
			_provider.Setup(p => p.GetService(typeof(IArbitrageContextApi))).Returns(arbitrageLoadContext.Object);
			var api = new ArbitrageLoadApi(_provider.Object);
			var list = api.GetClaimStatistic(new ClaimStatRequest()
			{
				StartDate = null,
				EndDate = null,
				Okved = okved,
				Region = region
			});
			list.ShouldHaveSingleItem();
		}

	}
}